<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$id = $_POST['id'];
$retorno = "<select class='form-select' id='modi_agrupacion_cliente'>";

$statement = $conexion->prepare("SELECT id, codigo_corto, cliente, rfc, vencimiento, id_clasificacion FROM clientes WHERE id = ?");
$statement->bind_param("i",$id);
$statement->execute();
$resultados = $statement->get_result();

// $respuesta = [];

while($fila = $resultados->fetch_assoc()){
    $info = [
        'id'		        => $fila['id'],
        'codigo_corto'       => $fila['codigo_corto'],
        'cliente'       => $fila['cliente'],
        'rfc'       => $fila['rfc'],
        'vencimiento'       => $fila['vencimiento'],
        'id_clasificacion'       => $fila['id_clasificacion']
    ];
    // array_push($respuesta, $info);
    
}


echo json_encode($info);
// echo $retorno;

?>