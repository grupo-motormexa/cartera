<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$id_usuario = $_POST['id_usuario'];

$statement = $conexion->prepare("SELECT p.id, p.permiso, fu.departamento FROM permisos p LEFT JOIN `filtros_usuarios` fu ON fu.id_usuario = p.usuario WHERE p.usuario = ?");
$statement->bind_param("s",$id_usuario);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];

while($fila = $resultados->fetch_assoc()){

    if($fila['departamento'] == NULL){
        $valores = NULL;
    }else{
        $valores = explode(",",$fila['departamento']);
    }
    $info = [
        'id'		        => $fila['id'],
        'permiso'       => $fila['permiso'],
        'filtro' =>     $valores
    ];
    array_push($respuesta, $info);
}



echo json_encode($respuesta);

?>