<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

// $id_equipo = $_POST['id_equipo'];

$statement = $conexion->prepare("SELECT a.id, a.num_cliente AS codigo, a.codigo_corto, a.cliente, a.rfc, a.vencimiento, a.id_clasificacion, b.nombre FROM clientes a 
LEFT JOIN agrupacion b ON b.id = a.id_clasificacion
ORDER BY a.cliente asc");
// $statement->bind_param("i",$id_equipo);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];

while($fila = $resultados->fetch_assoc()){
    $info = [
        'id'	            => $fila['id'],
        'codigo'	        => $fila['codigo'],
        'codigo_corto'	    => $fila['codigo_corto'],
        'cliente'           => $fila['cliente'],
        'rfc'               => $fila['rfc'],
        'vencimiento'       => $fila['vencimiento'],
        'agrupacion'       => $fila['id_clasificacion']."-".$fila['nombre']
    ];
    array_push($respuesta, $info);
}


echo json_encode($respuesta);

?>