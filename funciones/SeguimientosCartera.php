<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$id_factura = $_GET['id_factura'];

$statement = $conexion->prepare("SELECT usuario, fecha, comentario FROM comentarios WHERE id_factura = ? ORDER BY fecha DESC");
$statement->bind_param("i",$id_factura);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];

while($fila = $resultados->fetch_assoc()){
    $info = [
        'usuario'		        => $fila['usuario'],
        'fecha'                 => $fila['fecha'],
        'comentario'            => $fila['comentario']
    ];
    array_push($respuesta, $info);
}


echo json_encode($respuesta);

?>