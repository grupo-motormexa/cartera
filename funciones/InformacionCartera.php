<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

function add_caracteres( $cadena1, $cadena2, $posicion) {

    $nueva = '';
    $cadena_general = '';
    $l=0;

    for( $n=0; $n < strlen( $cadena1 )+$posicion; $n++ ) {
        
        if( $l == $posicion) {
            $nueva .= $cadena_general.$cadena2;
            $cadena_general = '';
            $l=0;
            
        }
        $cadena_general .= $cadena1[$n];
        $l++;
    }
    $nueva = substr($nueva, 0, -1);
    return $nueva;
    
}

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$id = $_POST['id'];

$statement = $conexion->prepare("SELECT b.cuenta AS cuenta_contable, a.fecha_recibida, a.fecha_portal, a.fecha_devuelta, a.estatus, a.fecha_aprox_pago, a.fecha_pago,a.contra_recibo, a.zona AS zona_factura, a.estatus_especial FROM factura a
LEFT JOIN cuentas b ON b.id_empresa = a.id_empresa AND b.serie = a.serie AND b.codigo_corto = a.codigo_corto
WHERE a.id = ?");
$statement->bind_param("i",$id);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];

while($fila = $resultados->fetch_assoc()){

    $cuenta_contable = add_caracteres($fila['cuenta_contable'], '-', 4);

    $info = [
        'cuenta_contable'		    => $cuenta_contable,
        'fecha_recibida'		    => $fila['fecha_recibida'],
        'fecha_portal'		        => $fila['fecha_portal'],
        'fecha_devuelta'              => $fila['fecha_devuelta'],
        'estatus'                   => $fila['estatus'],
        'fecha_aprox_pago'          => $fila['fecha_aprox_pago'],
        'fecha_pago'                 => $fila['fecha_pago'],
        'numero_contrarecibo'               =>$fila['contra_recibo'],
        'zona_factura'               =>$fila['zona_factura'],
        'estatus_especial'            =>$fila['estatus_especial']
    ];
    // array_push($respuesta, $info);
}


echo json_encode($info);

?>