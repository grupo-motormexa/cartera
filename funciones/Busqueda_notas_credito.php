<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$factura = "%".$_POST['factura']."%";
// echo $factura;

$statement = $conexion->prepare("SELECT nc.id,  IF(f.id_empresa = '01','GRUPO','AUTO') AS empresa, CONCAT(f.serie,f.folio) AS factura, e.cliente AS cliente, e.rfc AS rfc, CONCAT(nc.serie,nc.folio) AS nota_credito, nc.fecha AS fecha, nc.importe AS importe FROM `nota_credito` nc
LEFT JOIN factura f ON f.id = nc.id_factura
LEFT JOIN (
  SELECT
    codigo_corto,
	cliente,
	rfc,
    MIN(id) AS id
  FROM clientes
  GROUP BY codigo_corto
) e ON e.codigo_corto = f.codigo_corto
WHERE f.folio LIKE ?");
$statement->bind_param("s",$factura);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];

while($fila = $resultados->fetch_assoc()){
    $info = [
        'id'		        => $fila['id'],
        'empresa'       => $fila['empresa'],
        'factura'       => $fila['factura'],
        'cliente'       => $fila['cliente'],
        'rfc'       => $fila['rfc'],
        'nota_credito'       => $fila['nota_credito'],
        'fecha'       => $fila['fecha'],        
        'importe'       => $fila['importe']
    ];
    array_push($respuesta, $info);
}


echo json_encode($respuesta);

?>