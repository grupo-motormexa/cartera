<?php
require('../funciones.php');

// error_reporting(0);
// header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$id_usuario = $_POST['id_usuario'];
$permisos_cartera = implode(",",$_POST['permisos_cartera']);
$permiso_edicion = $_POST['permiso_edicion'];


$statement = $conexion->prepare("DELETE FROM filtros_usuarios WHERE id_usuario = ?");
$statement->bind_param("s", $id_usuario);
$statement->execute();
// $resultados = $statement->get_result();

if($permisos_cartera != null || $permisos_cartera != ''){
    $statement = $conexion->prepare("INSERT INTO filtros_usuarios (id_usuario, departamento) VALUES (?,?)");
    $statement->bind_param("ss", $id_usuario, $permisos_cartera);
    $statement->execute();
}


$statement = $conexion->prepare("SELECT id FROM permisos WHERE usuario = ?");
$statement->bind_param("s", $id_usuario);
$statement->execute();
$resultados = $statement->get_result();

$row_cnt = $resultados->num_rows;

if($permiso_edicion == 'SI'){
    $id_permiso = 1;
}else{
    $id_permiso = 2;
}

if($row_cnt == 0){

    $statement = $conexion->prepare("INSERT INTO permisos (usuario, permiso) VALUES (?,?)");
    $statement->bind_param("si", $id_usuario, $id_permiso);
    $statement->execute();

}else{

    $statement = $conexion->prepare("UPDATE permisos SET permiso = ? WHERE usuario = ?");
    $statement->bind_param("is", $id_permiso, $id_usuario);
    $statement->execute();

}

echo 1;

?>