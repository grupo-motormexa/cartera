<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionNew();
$conexion->set_charset('utf8');

// $id_equipo = $_POST['id_equipo'];
if(isset($_POST['estatus']) && isset($_POST['menu'])){
    $estatus = $_POST['estatus'];
    $menu = $_POST['menu'];
    $statement = $conexion->prepare("SELECT u.id, u.nombre, u.apellidop, u.apellidom, u.id_sucursal, u.usuario, u.contrasena, u.estatus FROM usuarios u INNER JOIN menu_usuario mu ON mu.id_usuario = u.id LEFT JOIN menu m ON m.id = mu.id_menu WHERE u.estatus = ? AND m.nombre_menu = ?");
    $statement->bind_param("ss",$estatus,$menu);
}else if(isset($_POST['estatus'])){
    $estatus = $_POST['estatus'];
    $statement = $conexion->prepare("SELECT id, nombre, apellidop, apellidom, id_sucursal, usuario, contrasena, estatus FROM usuarios WHERE estatus = ?");
    $statement->bind_param("s",$estatus);
}else{
    $statement = $conexion->prepare("SELECT id, nombre, apellidop, apellidom, id_sucursal, usuario, contrasena, estatus FROM usuarios");
    // $statement->bind_param("i",$id_equipo);
}

// $statement = $conexion->prepare("SELECT id, nombre, apellidop, apellidom, id_sucursal, usuario, contrasena, estatus FROM usuarios");
// $statement->bind_param("i",$id_equipo);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];

while($fila = $resultados->fetch_assoc()){
    $info = [
        'id'		        => $fila['id'],
        'nombre'       => $fila['nombre'],
        'apellidop'       => $fila['apellidop'],
        'apellidom'       => $fila['apellidom'],
        'sucursal'       => $fila['id_sucursal'],
        'usuario'       => $fila['usuario'],
        'contrasena'       => $fila['contrasena'],
        'estatus' => $fila['estatus']
    ];
    array_push($respuesta, $info);
}


echo json_encode($respuesta);

?>