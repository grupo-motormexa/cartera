<?php
require('../funciones.php');

// error_reporting(0);
// header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$fecha = $_POST['fecha'];

$statement = $conexion->prepare("SELECT id_empresa, serie, count(fecha_fiscal) AS cantidad, SUM(importe_total) AS total FROM factura WHERE fecha_fiscal = ? GROUP BY fecha_fiscal, serie, id_empresa ORDER BY id_empresa");
$statement->bind_param("s",$fecha);
$statement->execute();
$resultados = $statement->get_result();
$total = 0;
$cantidad = 0;
$respuesta = "";

while($fila = $resultados->fetch_assoc()){
    // $info = [
    //     'id'		        => $fila['id'],
    //     'nombre'       => $fila['descripcion']
    // ];
    // array_push($respuesta, $info);

    if($fila['id_empresa'] == '01'){
        $empresa = 'GRUPO';
    }else{
        $empresa = 'AUTO';
    }

    $respuesta .= "<tr><td>".$empresa."</td><td>".$fila['serie']."</td><td>".$fila['cantidad']."</td><td align='right'>".number_format($fila['total'], 2, '.', ',')."</td></tr>";
    $total += $fila['total']; 
    $cantidad += $fila['cantidad']; 
}

$respuesta .= "<tr><td colspan='2' align='center'><b>TOTAL</b></td><td><b>".$cantidad."</b></td><td align='right'><b>".number_format($total, 2, '.', ',')."</b></td></tr></tbody></table>";

$respuesta_final = "<br><h4>TOTAL PARA EL DÍA <b>".$fecha."</b><br>TOTAL $ <b>".number_format($total, 2, '.', ',')."</b></h4><br><br><table class='table table-sm table-striped'><thead><tr><th>EMPRESA</th><th>SERIE</th><th>CANTIDAD</th><th align='right'>TOTAL</th></tr></thead><tbody>".$respuesta;


echo $respuesta_final;
// echo json_encode($respuesta);


?>