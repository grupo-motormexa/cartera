<?php
session_start();
require('../funciones.php');


// error_reporting(0);
// header('Content-type: application/json; charset=utf-8');

$modo = $_POST['modo'];

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

if ($conexion->connect_error){
    die("Connection failed: " . $conexion->connect_error);

}else{
    switch($modo){
        case 'Alta':

            $id = $_POST['id'];
            $comentario = strtoupper($_POST['comentario']);
            $usuario = $_SESSION['usuario'];

            $statement = $conexion->prepare("INSERT INTO comentarios (usuario,id_factura,comentario) VALUES (?,?,?)");
            $statement->bind_param("sis",$usuario,$id,$comentario);   
            $statement->execute();

            echo 1;

        break;

        case 'Baja':


        break;

        case 'Modificar':

            $id = $_POST['id'];
            $fecha_portal = $_POST['fecha_portal'];
            $fecha_devuelta = $_POST['fecha_devuelta'];
            $fecha_aprox_pago = $_POST['fecha_aprox_pago'];
            $fecha_pago = $_POST['fecha_pago'];
            $fecha_recibida = $_POST['fecha_recibida'];
            $estatus = $_POST['estatus'];
            $contra_recibo=$_POST['contra_recibo']; 
            $zona_factura = $_POST['zona_factura'];
            $estatus_especial = $_POST['estatus_especial'];
            

            if($fecha_portal == ''){
                $fecha_portal = null;
            }
            if($fecha_devuelta == ''){
                $fecha_devuelta = null;
            }
            if($fecha_aprox_pago == ''){
                $fecha_aprox_pago = null;
            }
            if($fecha_pago == ''){
                $fecha_pago = null;
            }
            if($fecha_recibida == ''){
                $fecha_recibida = null;
            }
            if ($contra_recibo==''){
                $contra_recibo=null;
            }
            if ($zona_factura==''){
                $zona_factura=null;
            }
            if ($estatus_especial==''){
                $estatus_especial=null;
            }
            $statement = $conexion->prepare("UPDATE factura SET fecha_recibida = ?, fecha_portal = ?, fecha_devuelta = ?, fecha_aprox_pago = ?, fecha_pago = ?, estatus = ?, contra_recibo= ?, zona =?, estatus_especial =?  WHERE id = ?");
            $statement->bind_param("sssssisssi",$fecha_recibida,$fecha_portal,$fecha_devuelta,$fecha_aprox_pago,$fecha_pago,$estatus,$contra_recibo,$zona_factura,$estatus_especial,$id);   
            $statement->execute();
            /*$statement = $conexion->prepare("UPDATE factura SET fecha_recibida = ?, fecha_portal = ?, fecha_devuelta = ?, fecha_aprox_pago = ?, fecha_pago = ?, estatus = ? WHERE id = ?");
            $statement->bind_param("sssssii",$fecha_recibida,$fecha_portal,$fecha_devuelta,$fecha_aprox_pago,$fecha_pago,$estatus,$id);   
            $statement->execute();*/
            
            echo 1;

        break;



    }
}

$statement = null;
$conexion = null;


?>