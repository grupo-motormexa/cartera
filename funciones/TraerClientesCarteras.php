<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

// $id_equipo = $_POST['id_equipo'];

$statement = $conexion->prepare("SELECT codigo_corto, CONCAT(codigo_corto,'- ',cliente) AS cliente_nombre FROM clientes GROUP BY codigo_corto ORDER BY cliente asc");
// $statement->bind_param("i",$id_equipo);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];
array_push($respuesta, [
    'id'		        => '',
    'descripcion'       => ''
]);

while($fila = $resultados->fetch_assoc()){
    $info = [
        'id'		        => $fila['codigo_corto'],
        'descripcion'       => $fila['cliente_nombre']
    ];
    array_push($respuesta, $info);
}


echo json_encode($respuesta);

?>