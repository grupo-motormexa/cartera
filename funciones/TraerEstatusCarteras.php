<?php
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

// $id_equipo = $_POST['id_equipo'];

$statement = $conexion->prepare("SELECT id, nombre, estatus FROM estatus");
// $statement->bind_param("i",$id_equipo);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];
array_push($respuesta, [
    'id'		        => '',
    'descripcion'       => ''
]);

while($fila = $resultados->fetch_assoc()){
    $info = [
        'id'		        => $fila['id'],
        'descripcion'       => $fila['nombre'],
        'estatus'       => $fila['estatus']
    ];
    array_push($respuesta, $info);
}


echo json_encode($respuesta);

?>