<?php
require('../funciones.php');

// error_reporting(0);
// header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$fecha_actual = date("Y-m-d");
$fecha_ultima = date("Y-m-d",strtotime($fecha_actual."- 1 year"));

if ($conexion->connect_errno) {
    $respuesta = [
        'error' => true
    ];
} else {

    $conexion->set_charset('utf8');



    $statement = $conexion->prepare("SELECT a.fecha_fiscal AS id, a.fecha_fiscal as start, a.fecha_fiscal as end, COUNT(a.fecha_fiscal) AS title FROM `factura` a WHERE a.fecha_fiscal > ? GROUP BY a.fecha_fiscal");
    $statement->bind_param("s",$fecha_ultima);
    $statement->execute();
    $resultados = $statement->get_result();

    $respuesta = [];

    while ($fila = $resultados->fetch_assoc()) {
        $respuesta[] = $fila;
    }
    // $resultados = $statement->get_result();
    echo json_encode($respuesta);
    exit;
}


$statement->close();
$conexion->close();
