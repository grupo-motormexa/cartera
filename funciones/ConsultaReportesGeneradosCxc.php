<?php
require('../funciones.php');

// error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');

$modo = $_GET['modo'];

if(isset($_GET['tipo'])){
    $filtro = "WHERE tipo = ".$_GET['tipo'];
}else{
    $filtro = "";
}

switch($modo){
    case 'Consulta':

        $statement = $conexion->prepare("SELECT id, estatus, usuario, empresa, mes, fecha_solicitado, fecha_generado, url_archivo FROM estatus_reportes $filtro ORDER BY id DESC LIMIT 0, 5");
        // $statement->bind_param("i",$id);
        $statement->execute();
        $resultados = $statement->get_result();

        $respuesta = [];

        while($fila = $resultados->fetch_assoc()){

            if($fila['url_archivo'] != null){
                $boton = "<a class='btn btn-success btn-sm' href='".$fila['url_archivo']."' >Descargar</a>";
            }else{
                $boton = "";
            }

            $info = [
                'id'		      => $fila['id'],
                'estatus'         => $fila['estatus'],
                'usuario'             => $fila['usuario'],
                'empresa'             => $fila['empresa'],
                'mes'             => $fila['mes'],
                'fecha_solicitado'    => $fila['fecha_solicitado'],
                'fecha_generado'              => $fila['fecha_generado'],
                'url_archivo'           =>  $boton,
            ];
            array_push($respuesta, $info);
        }

        echo json_encode($respuesta);
    break;
    case 'Consulta2':

        $statement = $conexion->prepare("SELECT count(*) AS total FROM estatus_reporte_gastos WHERE url_archivo = NULL");
        // $statement->bind_param("i",$id);
        $statement->execute();
        $resultados = $statement->get_result();

        $respuesta = [];

        while($fila = $resultados->fetch_assoc()){
            $info[0] = $fila['total'];
        }

        echo json_encode($info);
    break;
}
    

$statement = null;
$conexion = null;


?>