<?php
session_start();
require('../funciones.php');

error_reporting(0);
header('Content-type: application/json; charset=utf-8');

$conexion = ConexionCartera2024();
$conexion->set_charset('utf8');


function armarfiltro($filtro)
{
    $resultado = "";
    $primer = 0;
    foreach ($filtro as $key => $value) {

        if ($value != ""  && $primer == 0) {
            // echo $value;
            switch ($key) {
                case 'factura_bbj':
                    $resultado = "WHERE b.factura_bbj LIKE '%$value'";
                    break;
                case 'id_clasificacion':
                    $resultado = "WHERE g.id IN ($value)";
                    break;
                case 'id_tipo_cartera':
                    if ($value == 'Vigente') {
                        $resultado = "WHERE (DATE_ADD(b.fecha_fiscal, INTERVAL e.vencimiento DAY) > CURDATE() AND b.estatus NOT IN (4,5,8,9))";
                    } else {
                        $resultado = "WHERE (DATE_ADD(b.fecha_fiscal, INTERVAL e.vencimiento DAY) < CURDATE() AND b.estatus NOT IN (4,5,8,9))";
                    }
                    break;
                case 'empresa':
                    $resultado = "WHERE b.id_empresa = '$value'";
                    break;
                case 'sucursal':
                    $resultado = "WHERE RIGHT(b.serie,1) = '$value'";
                    break;
                case 'tipo':
                    $resultado = "WHERE LEFT(b.serie,1) IN ($value)";
                    break;
                case 'vendedor':
                    $resultado = "WHERE b.vendedor = '$value'";
                    break;
                case 'cliente':
                    $resultado = "WHERE b.codigo_corto IN ($value)";
                    break;
                case 'estatus':
                    $resultado = "WHERE b.estatus IN ($value)";
                    break;
                case 'pedido':
                    $resultado = "WHERE a.num_pedido = '$value'";
                    break;
                case 'factura':
                    $resultado = "WHERE b.folio LIKE '%$value'";
                    break;
                case 'fecha_inicial':
                    $resultado = "WHERE b.fecha_fiscal >= '$value'";
                    break;
                case 'fecha_final':
                    $resultado = "WHERE b.fecha_fiscal <= '$value'";
                    break;
                case 'factura_inicial':
                    $resultado = "WHERE RIGHT(b.folio,6) >= $value";
                    break;
                case 'factura_final':
                    $resultado = "WHERE RIGHT(b.folio,6) <= $value";
                    break;
            }
            $primer++;
        } else if ($value != "") {

            switch ($key) {
                case 'factura_bbj':
                    $resultado .= " AND b.factura_bbj LIKE '%$value'";
                    break;
                case 'id_clasificacion':
                    $resultado .= " AND g.id IN ($value)";
                    break;
                case 'id_tipo_cartera':
                    if ($value == 'Vigente') {
                        $resultado .= " AND (DATE_ADD(b.fecha_fiscal, INTERVAL e.vencimiento DAY) < CURDATE() AND b.estatus NOT IN (4,5,8,9))";
                    } else {
                        $resultado .= " AND (DATE_ADD(b.fecha_fiscal, INTERVAL e.vencimiento DAY) > CURDATE() AND b.estatus NOT IN (4,5,8,9))";
                    }
                    // $resultado .= " AND id_tipo_cartera = '$value'";
                    break;
                case 'empresa':
                    $resultado .= " AND b.id_empresa = '$value'";
                    break;
                case 'sucursal':
                    $resultado .= " AND RIGHT(b.serie,1) = '$value'";
                    break;
                case 'tipo':
                    $resultado .= " AND LEFT(b.serie,1) IN ($value)";
                    break;
                case 'vendedor':
                    $resultado .= " AND b.vendedor = '$value'";
                    break;
                case 'cliente':
                    $resultado .= " AND b.codigo_corto IN ($value)";
                    break;
                case 'estatus':
                    $resultado .= " AND b.estatus IN ($value)";
                    break;
                case 'pedido':
                    $resultado .= " AND a.num_pedido = '$value'";
                    break;
                case 'factura':
                    $resultado .= " AND b.folio LIKE '%$value'";
                    break;
                case 'fecha_inicial':
                    $resultado .= " AND b.fecha_fiscal >= '$value'";
                    break;
                case 'fecha_final':
                    $resultado .= " AND b.fecha_fiscal <= '$value'";
                    break;
                case 'factura_inicial':
                    $resultado .= " AND RIGHT(b.folio,6) >= $value";
                    break;
                case 'factura_final':
                    $resultado .= " AND RIGHT(b.folio,6) <= $value";
                    break;
            }
        }
    }

    return $resultado;
}

$usuario = $_SESSION['usuario'];
$meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
$filtro = [];
$empresa = $_GET['empresa'];
$sucursal = $_GET['sucursal'];
$tipo = $_GET['tipo'];
$vendedor = $_GET['vendedor'];
$cliente = $_GET['cliente'];
$estatus = $_GET['estatus'];
$pedido = $_GET['pedido'];
$factura = $_GET['factura'];
$fecha_inicial = $_GET['fecha_inicial'];
$fecha_final = $_GET['fecha_final'];
$id_clasificacion = $_GET['id_clasificacion'];
$id_tipo_cartera = $_GET['id_tipo_cartera'];
$factura_bbj = $_GET['factura_bbj'];
$factura_inicial = intval($_GET['factura_inicial']);
$factura_final = intval($_GET['factura_final']);

if ($tipo != "") {
    $tipo = "'" . str_replace(",", "','", $tipo) . "'";
} else {
    // $tipo = "";

    $busqueda = "";

    $statement_carteras= $conexion->prepare("SELECT departamento FROM filtros_usuarios WHERE id_usuario = ?");
    $statement_carteras->bind_param("s",$usuario);
    $statement_carteras->execute();
    $resultados_cartera = $statement_carteras->get_result();
    $fila5 = $resultados_cartera->fetch_assoc();

    $pos1 = strpos($fila5['departamento'],'Refacciones');
    if ($pos1 !== false) {
        $busqueda .= "'R',";
    }
    $pos2 = strpos($fila5['departamento'],'Servicio');
    if ($pos2 !== false) {
        $busqueda .= "'S',";
    }
    $pos3 = strpos($fila5['departamento'],'Body');
    if ($pos3 !== false) {
        $busqueda .= "'B',";
    }
    $pos4 = strpos($fila5['departamento'],'Garantia');
    if ($pos4 !== false) {
        $busqueda .= "'G',";
    }

    $tipo = substr($busqueda, 0, -1);
}

if ($id_tipo_cartera != "") {
    $id_tipo_cartera = "'" . str_replace(",", "','", $tipo) . "'";
} else {
    $id_tipo_cartera = "";
}

// if (count($estatus) > 0) {
//     for ($i = 0; $i < count($estatus); $i++) {
//         $estatus_real .= $estatus[$i] . ",";
//     }
//     $estatus_real = substr($estatus_real, 0, -1);
// }
$filtro = array('factura_bbj' => $factura_bbj, 'id_clasificacion' => $id_clasificacion, 'id_tipo_cartera' => $id_tipo_cartera, 'empresa' => $empresa, 'sucursal' => $sucursal, 'tipo' => $tipo, 'vendedor' => $vendedor, 'cliente' => $cliente, 'estatus' => $estatus, 'pedido' => $pedido, 'factura' => $factura, 'fecha_inicial' => $fecha_inicial, 'fecha_final' => $fecha_final, 'factura_inicial' => $factura_inicial, 'factura_final' => $factura_final);

$filtro_final = armarfiltro($filtro);

$consulta = "SELECT b.id, d.nombre_corto AS vendedor, IF(b.id_empresa='01','GRUPO','AUTO') AS id_empresa, b.fecha_fiscal, YEAR(b.fecha_fiscal) AS ano, MONTH(b.fecha_fiscal)-1 AS mes, e.rfc, 
CASE 
WHEN DATE_ADD(b.fecha_fiscal, INTERVAL e.vencimiento DAY) < CURDATE() AND b.estatus NOT IN (4,5,8,9) THEN 'Vencida'
WHEN DATE_ADD(b.fecha_fiscal, INTERVAL e.vencimiento DAY) > CURDATE() AND b.estatus NOT IN (4,5,8,9) THEN 'Vigente'
WHEN b.estatus IN (4,5,8,9) THEN 'Liquidada'
ELSE 'Vigente' 
END AS id_tipo_cartera, 
g.nombre AS id_clasificacion, b.importe_total AS importe_total, b.abonos AS abono, IFNULL(nc.nota,0) AS nota_credito, (b.importe_total-b.abonos-IFNULL(nc.nota,0)) AS saldo, CONCAT(b.codigo_corto,'- ',e.cliente) AS nombre_cliente, b.serie, b.folio, b.factura_bbj, b.fecha_pago, b.fecha_portal, b.fecha_aprox_pago AS fecha_aprox_pago, c.nombre AS estatus, IFNULL(a.id_factura,b.id) AS id_factura, IFNULL(a.num_pedido,'-') AS num_pedido, IFNULL(a.siniestro,'-') AS siniestro, IFNULL(a.fecha_pedido,'-') AS fecha_pedido, (a.importe*1.16) AS importe_pedido, f.comentario, b.contra_recibo, b.zona FROM pedidos a
RIGHT JOIN factura b ON b.id = a.id_factura
LEFT JOIN vendedores d ON d.clave = b.vendedor
LEFT JOIN (SELECT id_factura, SUM(importe) AS nota FROM nota_credito GROUP BY id_factura) nc ON nc.id_factura = b.id
INNER JOIN estatus c ON b.estatus = c.id
LEFT JOIN (
  SELECT
    codigo_corto,
	cliente,
	rfc,
    vencimiento,
    id_clasificacion,
    MIN(id) AS id
  FROM clientes
  GROUP BY codigo_corto
) e ON e.codigo_corto = b.codigo_corto
LEFT JOIN ultimo_seguimiento f ON f.id_factura = b.id
LEFT JOIN agrupacion g ON g.id = e.id_clasificacion
$filtro_final
ORDER BY b.id ASC";

// echo $consulta;

$statement = $conexion->prepare($consulta);
$statement->execute();
$resultados = $statement->get_result();

$respuesta = [];
$ids_facturas = [];
$id_valor = 0;
$table = 0;
$contador_filas = 0;
$info = array();

$consulta_permiso = "SELECT permiso FROM permisos WHERE usuario = '$usuario'";
$statement_permiso = $conexion->prepare($consulta_permiso);
$statement_permiso->execute();
$resultados_permiso = $statement_permiso->get_result();
$fila2 = $resultados_permiso->fetch_assoc();


while ($fila = $resultados->fetch_assoc()) {

    $llena_complemetos = 0;
    $mes = $meses[$fila['mes']];

    if($fila['ano'] == '2022' && intval($fila['mes']) < 7){
        $factura = substr(str_repeat(0, 7) . $fila['factura_bbj'], -7);
        $nombre =  $fila['serie'] . $factura;
    }else{
        $factura = substr(str_repeat(0, 7) . $fila['folio'], -7);
        if ($fila['id_empresa'] == 'GRUPO'){
            $nombre = "GMG090821RT0-" . $fila['serie'] . $factura;
        }else{
            $nombre = "AMO021114AG5-" . $fila['serie'] . $factura;
        }
    }

    $seriee = substr($fila['serie'], 0, 1);  // bcd
    if($seriee == 'S' || $seriee == 'B' || $seriee == 'G' ){
        $ser = 'srv';
    }
    if($seriee == 'R' ){
        $ser = 'ref';
    }

    // oficilmente es    efac\Generated\ref o srv\2023\mes\RFC

    if ($fila['id_empresa'] == 'GRUPO') {
        $archivo = "http://200.1.1.240/efacGpo/efac/Generated/" . $ser . "/" . $fila['ano'] . "/" . $mes . "/" . $fila['rfc'] . "/" . $nombre;
    } else {
        $archivo = "http://200.1.1.240/efacMit/efac/Generated/" . $ser . "/" . $fila['ano'] . "/" . $mes . "/" . $fila['rfc'] . "/" . $nombre;
    }
    // $opciones =$consulta;


    if ($fila2['permiso'] == 1) {
        $opciones = "<button type='button' class='btn btn-success btn_small btn-sm ver'><i class='fas fa-eye'></i></i></button><button type='button' class='btn btn-secondary btn_small btn-sm seguimiento'><i class='fas fa-forward'></i></i></button><a class='btn btn-sm btn-danger btn_small' target='_blank' href='" . $archivo . ".pdf' download disabled><i class='fas fa-file-pdf'></i></a><a class='btn btn-sm btn-dark btn_small' target='_blank' href='" . $archivo . ".xml' download disabled><i class='far fa-file-code'></i></a>";
    } else {
        $opciones = "<a class='btn btn-sm btn-danger btn_small' target='_blank' href='" . $archivo . ".pdf' download disabled><i class='fas fa-file-pdf'></i></a><a class='btn btn-sm btn-dark btn_small' target='_blank' href='" . $archivo . ".xml' download disabled><i class='far fa-file-code'></i></a>";
    }



    // $opciones = "<button type='button' class='btn btn-success btn_small btn-sm ver'><i class='fas fa-eye'></i></i></button><a class='btn btn-sm btn-danger btn_small' target='_blank' href='".$archivo.".pdf' download><i class='fas fa-file-pdf'></i></a><a class='btn btn-sm btn-dark btn_small' target='_blank' href='".$archivo.".xml' download><i class='far fa-file-code'></i></a>";

    // $opciones = "<button type='button' class='btn btn-success btn_small btn-sm ver'><i class='fas fa-eye'></i></i></button>";
    $id_factura = $fila['id_factura'];

    $tabla_ac = "<tr><td>" . $fila['num_pedido'] . "</td><td>" . $fila['siniestro'] . "</td><td>" . $fila['fecha_pedido'] . "</td><td>" . number_format($fila['importe_pedido'], 2, '.', ',') . "</td></tr>";
    if($fila['num_pedido'] != "-"){
        $pedidos_coma = " | ".$fila['num_pedido'];
    }else{
        $pedidos_coma = "";
    }

    $info[$id_valor] = array(
        'id'             => $fila['id'],
        'empresa'        => $fila['id_empresa'],
        'vendedor'       => $fila['vendedor'],
        'fecha'          => $fila['fecha_fiscal'],
        'importe_total'  => $fila['importe_total'],
        'abono'          => $fila['abono'],
        'nota_credito'   => $fila['nota_credito'],
        'saldo'          => $fila['saldo'],
        'factura'        => $fila['serie'] . $fila['folio'],
        'factura_bbj'    => $fila['factura_bbj'],
        'cliente'        => substr($fila['nombre_cliente'], 0, 60),
        'fecha_pago'     => $fila['fecha_pago'], 
        'fecha_portal'   => $fila['fecha_portal'],
        'contra_recibo' => $fila['contra_recibo'],
        'estatus'        => $fila['estatus'],
        'comentario'     => $fila['comentario'],
        'zona'           => $fila['zona'],
        'siniestro'       => $fila['siniestro'],
        'pedido'           => $pedidos_coma,
        'fecha_aprox_pago'     => $fila['fecha_aprox_pago'],
        'id_clasificacion'     => $fila['id_clasificacion'],
        'id_tipo_cartera'     => $fila['id_tipo_cartera'],
        'opciones'       => $opciones,
        'pedidos'        => $tabla_ac,
        'complementos'   => ""
    );

    if ($id_valor > 0) {

        if ($id_factura != $info[$id_valor - 1]['id']) {

            $pedido = $info[$id_valor - 1]['pedidos'];
            $ped = $info[$id_valor - 1]['pedido'];

            if($ped != "" ){
                $ped2 = $ped . " | ";
            }else{
                $ped2 = "";
            }
            

            $pedido2 = "<tr><td>PEDIDO</td><td>SINIESTRO</td><td>FECHA</td><td>IMPORTE</td></tr>" . $pedido;
            $info[$id_valor - 1]['pedidos'] = $pedido2;

            $info[$id_valor - 1]['pedido'] = $ped2;

            array_push($respuesta, $info[$id_valor - 1]);


        } else {

            $info[$id_valor]['pedidos'] .= $info[$id_valor - 1]['pedidos'];
            $info[$id_valor]['pedido'] .= $info[$id_valor - 1]['pedido'];
        }
    }

    if($llena_complemetos == 0){

        $id_fac = $fila['id'];
        $consulta_complementos = "SELECT SERIECP, ID_COMPAGO, IMPORTE, FECCP FROM complementos_pago WHERE id_factura = $id_fac";
        $statement_complemento = $conexion->prepare($consulta_complementos);
        $statement_complemento->execute();
        $resultados_complemento = $statement_complemento->get_result();

        $tabla_complementos_html = "<tr><td>SERIE</td><td>FOLIO</td><td>IMPORTE</td><td>FECHA</td><td>PDF</td><td>XML</td></tr>";



        while ($fila3 = $resultados_complemento->fetch_assoc()) {

            if ($fila['id_empresa'] == 'GRUPO'){
                $nombre2 = "GMG090821RT0-" . $fila3['SERIECP'] . $fila3['ID_COMPAGO'];;
            }else{
                $nombre2 = "AMO021114AG5-" . $fila3['SERIECP'] . $fila3['ID_COMPAGO'];;
            }

            $archivo_complemento = "http://200.1.1.240/efacGpo/efac/Generated/" . $fila['ano'] . "/" . $mes . "/" . $fila['rfc'] . "/" . $nombre2;

            $boton_complemento_pdf = "<a class='btn btn-sm btn-danger btn_small' target='_blank' href='" . $archivo_complemento . ".pdf' download disabled><i class='fas fa-file-pdf'></i></a>";
            $boton_complemento_xml = "<a class='btn btn-sm btn-dark btn_small' target='_blank' href='" . $archivo_complemento . ".xml' download disabled><i class='far fa-file-code'></i></a>";

            $tabla_complementos_html .= "<tr><td>".$fila3['SERIECP']."</td><td>".$fila3['ID_COMPAGO']."</td><td>".$fila3['IMPORTE']."</td><td>".$fila3['FECCP']."</td><td>".$boton_complemento_pdf."</td><td>".$boton_complemento_xml."</td></tr>";

        }

        $tabla_complementos_html .= "<tr>";

        $info[$id_valor]['complementos']  = $tabla_complementos_html;

        $llena_complemetos = 1;
    }


    $id_valor++;
}

if ($id_valor > 0) {
    array_push($respuesta, $info[$id_valor - 1]);
}


echo json_encode($respuesta);

?>