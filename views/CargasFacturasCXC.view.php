<div class="card">
  <div class="card-header">
    <h5 class="card-title">Facturas Cargadas por día CXC</h5>
  </div>
  <div class="card-body">
  <br>
    <div class="full-container">
      <div class="row">
        <div class="col-md-8">
          <div id="calendario"></div>
        </div>
        <div class="col-md-4">
          <div id="detalle" class="table-responsive"></div>
        </div>
      </div>
    </div>


    <div id="cargar_informacion_cuenta_iva">
    </div>

  </div>
</div>



<script type="text/javascript">
$(document).ready(function() {
  // Configuración del calendario
  var calendarEl = document.getElementById('calendario');
  var calendar = new FullCalendar.Calendar(calendarEl, {
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'agendaWeek,agendaDay'
    },
    locale: 'es',
    timeZone: 'America/Mexico',
    editable: false,
    selectable: false,
    allDaySlot: false,
    events: "funciones/TraerFacturasCargadasCxc.php",
    // dateClick: function(date, jsEvent, view) {
    //   // Actualizar el detalle con el total correspondiente a la fecha seleccionada
    //   // var total = calcularTotal(date);
    //   $('#detalle').html('<h3>Total para el día '+date.format()+' 5555505</h3>');
    // },
    eventClick: function(info) { // when some one click on any event
                // starttime = moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
                // endtime = moment(info.event.end).format('HH:mm:00');
                // endtime = $.fullCalendar.moment(event.end).format('h:mm');
                // starttime = $.fullCalendar.moment(event.start).format('dddd, MMMM Do YYYY, h:mm');
                // var mywhen = starttime + ' - ' + endtime;
                // $('#modalTitle').html(event.title);
                // $('#modalWhen').text(mywhen);
                // $('#titulo').val(info.event.title);
                // $('#inicio').val(info.event.start);
                // $('#fin').val(info.event.end);
                // $('#organizador').val(info.event.organizador);
                // $('#comentarios').val(info.event.comentarios);
                // id = info.event.id;
                // $("#eventID").val(id);
                // $('#calendarModal').val(id);
                fecha = info.event.id;
                LlenarInfoFecha(fecha);
                
                

            },
  });

  calendar.render();
});


function LlenarInfoFecha(fecha){

  table = "";

  $.ajax({
            url: "funciones/TraerTablaDiaCxc.php",
            type: "POST",
            data: {
                "fecha" : fecha,
                // "inventario": inventario
            },
            success: function(datas) {
                // $("#resultado_reporte").html(datas);
                // $("#resultado_reporte").css("display", "block");
                $('#detalle').html(datas);
                // table = datas;
            }
        });

  // return table;
}


function formatCurrency(locales, currency, fractionDigits, number) {
  var formatted = new Intl.NumberFormat(locales, {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: fractionDigits
  }).format(number);
  return formatted;
}

</script>