<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

  <title>Grupo Motormexa Guadalajara, SA de CV</title>
 

  <!-- Bootstrap core CSS --><!-- Custom styles for this template -->
  <!-- <link href="css/signin.css" rel="stylesheet"> -->
  <link href="images/icon/agencia1.ico" rel="shortcut icon"/></link>
  <link href="css/bootstrap.min.css" rel="stylesheet"></link>
  <!-- <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap4.min.css"/> -->
  <link href="css/general.css" rel="stylesheet"></link>
  <link rel="stylesheet" href="css/component-chosen.css"></link>
  <!-- <link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap4.min.css" rel="stylesheet"></link> -->
  <!-- <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet"></link> -->
  <link href="css/jquery.dataTables.min.css" rel="stylesheet"></link>
  <!-- <link href="https://cdn.datatables.net/fixedcolumns/3.3.2/css/fixedColumns.dataTables.min.css" rel="stylesheet"></link> -->
  <link href="css/themes/default/style.css" rel="stylesheet"></link>



  <script src="js/jquery-3.5.1.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/jspdf.debug.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/autoNumeric.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/jstree.min.js"></script>
  <script src="js/bootstrap-notify.js"></script>
  <script src="js/SimpleAjaxUploader.js"></script>
  <script src="http://momentjs.com/downloads/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <link href='fullcalendar/lib/main.css' rel='stylesheet' />
  <script src='fullcalendar/lib/main.js'></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <!-- <script src="js/dataTables.bootstrap4.min.js"></script> -->
  <!-- <script src="js/dataTables.fixedColumns.min.js"></script> -->
  <script type="text/javascript" src="js/chosen.jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js"></script>
  <!-- <script type="text/javascript" src="js/jquery.mask.js"></script> -->

  <script type="text/javascript" src="sweetalert/package/dist/sweetalert2.all.js"></script>
 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>

  <script src="js/jspdf.plugin.autotable.js"></script>

  <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script> -->
  <link rel="stylesheet" href="css/all.css"></link>
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous"> -->

<!--tablas refacciones -->
<link href="https://unpkg.com/bootstrap-table@1.22.1/dist/bootstrap-table.min.css" rel="stylesheet">

<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.22.1/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.22.1/dist/bootstrap-table-locale-all.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.22.1/dist/extensions/export/bootstrap-table-export.min.js"></script>




    <style>
    @media print {
      header, footer, nav, aside {
        display: none;
      }body {
        font-size: 8px;
      } 
      @page { size: landscape; }
    }
.inputstyle{
  font-size: 9pt;
  margin: 10px;
}
.navbar-nav li:hover > ul.dropdown-menu {
    display: block;
}
.dropdown-submenu {
    position:relative;
}
.dropdown-submenu>.dropdown-menu {
    top:0;
    left:100%;
    margin-top:-6px;
}
.dropdown-submenu a::after{
    transform: rotate(-90deg);
    position: absolute;
    right: 3px;
    top: 40%;
}
.inicio{
  width: 0%;
}
.final{
  width: 100%;
}
.error {
  color: red;
  background-color: #FFF;
}
.animacion{
  transition: all 2s ease .5s;
} 
ul>li>a>span{
  padding-right: 10px;
}
.dt-body-right{
 text-align: right;
}
.align-center{
  text-align: center;
}
th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        /* width: 800px; */
        margin: 0 auto;
    }
    
.oculto{
  display: none;
}
textarea {
   resize: none;
}

.visible{
  display: block;
}

.good {
  color: green;
}

.wrong {
  color: red;
}
    </style>

<script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html lang="es" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta charset="utf-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        
        function printDiv(div) 
        {

            var divToPrint=document.getElementById(div);

            var newWin=window.open('','Print-Window');

            newWin.document.open();

            newWin.document.write('<html><head><style> header, footer, nav, aside {display: none;}table { font-size: 10px;  font-family: arial;}  @page { size: landscape; } </style></head><body onload="window.print();">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();

            setTimeout(function(){newWin.close();},20);

        }

    </script>

  </head>
  <body>
  <?php include("views/Menu.php"); ?> 

  <div class="container-fluid">

  <!-- <div class="menuContainer"></div> -->
  <div class="principal"></div>
  <div class="tabla" style="display: none;"></div>
  <div class="resultado"></div>
  <!-- <br> -->


</div>

</body>
<script src="js/bootstrap.min.js"></script>
<script>

    // $(document).ready(function () {
      
    //   $('.menuContainer').load('funciones/Menu.php');

    // });

    function actualizarestado(pagina){
        $("#div_tabla" ).css("display","none");
        $( "#div_tabla_unidades_conta" ).css("display","none");
        $( "#div_tabla_sueldos" ).css("display","none");
        $('.principal').load('/views/'+pagina);
        $('.datepicker').datepicker();
    }

		$('.dropdown-submenu > a').on("click", function(e) {
		    var submenu = $(this);
		    $('.dropdown-submenu .dropdown-menu').removeClass('show');
		    submenu.next('.dropdown-menu').addClass('show');
		    e.stopPropagation();
		});

		$('.dropdown').on("hidden.bs.dropdown", function() {
		    $('.dropdown-menu.show').removeClass('show');
		});




var idioma_espanol = {
  "decimal":        "",
  "emptyTable":     "No hay datos disponibles",
  "info":           "Viendo _START_ a _END_ de _TOTAL_ registros",
  "infoEmpty":      "Viendo 0 a 0 de 0 registros",
  "infoFiltered":   "(Filtrado de _MAX_ registros totales)",
  "infoPostFix":    "",
  "thousands":      ",",
  "lengthMenu":     "Ver _MENU_ registros",
  "loadingRecords": "Cargando...",
  "processing":     "Procensando...",
  "search":         "Buscar:",
  "zeroRecords":    "No se encontraron registros",
  "paginate": {
      "first":      "Primero",
      "last":       "Ultimo",
      "next":       "Siguiente",
      "previous":   "Anterior"
  },
  "aria": {
      "sortAscending":  ": activate to sort column ascending",
      "sortDescending": ": activate to sort column descending"
  }
}


 //----------------------------------------------- INICIA REPORTE DE RELACION DE GASTOS MENSUAL

 $(document).on('click', '#Boton_Buscar_gastos', function () {
      var empresa = $("#empresa_gastos").val();
      var ejercicio = $("#ejercicio_gastos").val();
      var periodo = $("#periodo_gastos").val();
      var tipo = $('input:radio[name=customRadioInline1]:checked').val();
      
      // id_cheque = (('00000' + cheque).slice(-5));

      // console.log("empresa: "+id_emp + " cheque: " +id_cheque);
      $("#resultados_gastos").html("<div class='d-flex justify-content-center'>\
  <div class='spinner-border' role='status'>\
    <span class='sr-only'>Loading...</span>\
  </div>\
</div>");
  $("#resultados_gastos").css("display","block");

          $.ajax({
            url: "funciones/ReporteGastosMensual.php",
            type: "POST",
            // dataType: "json",
            data: {"empresa":empresa,"ejercicio":ejercicio,"periodo":periodo,"estatus":1,"tipo":tipo},
            success: function(data) {
              //Si no encuentra ningun resultado
              // console.log(data);
              TraerResultadosReporteGastos();
            }
          });
});

function TraerResultadosReporteGastos(){

  var departamento = $("#departamento_gastos").val();
  var sucursal = $("#sucursal_gastos").val();
  var ejercicio = $("#ejercicio_gastos").val();
  var empresa = $("#empresa_gastos").val();
  var periodo_num =  $("#periodo_gastos").val();
  var periodo = $("#periodo_gastos option:selected").text();
  var sucursal_nombre = $("#sucursal_gastos option:selected").text();
  var departamento_nombre = $("#departamento_gastos option:selected").text();
  var tipo = $('input:radio[name=customRadioInline1]:checked').val();
  var mostrargasto = $("#mostrar_gastos").val();

  if(mostrargasto == 1){
    url_enviar = "funciones/ReporteGastosMensualTablaNew.php";
  }else{
    url_enviar = "funciones/ReporteGastosMensualTabla.php";
  }

  $.ajax({
      url: url_enviar,
      type: "POST",
      // dataType: "json",
      data: {"departamento":departamento,"sucursal":sucursal,"ejercicio":ejercicio,"empresa":empresa,"periodo":periodo,"periodo_num":periodo_num,"sucursal_nombre":sucursal_nombre,"departamento_nombre":departamento_nombre,"tipo":tipo},
      success: function(data) {
        //Si no encuentra ningun resultado
        // console.log(data);
        $("#resultados_gastos").html('');
        $("#resultados_gastos").html(data);
        // $("#resultados_gastos").css("display","block");

      }
  });

}

 //----------------------------------------------- INICIA REPORTE DE RELACION DE GASTOS MENSUAL

 $(document).on('click', '#Boton_Buscar_gastos_new', function () {
      var empresa = $("#empresa_gastos_new").val();
      var ejercicio = $("#ejercicio_gastos_new").val();
      var periodo = $("#periodo_gastos_new").val();
      var tipo = $('input:radio[name=customRadioInline1_new]:checked').val();
      
      // id_cheque = (('00000' + cheque).slice(-5));

      // console.log("empresa: "+id_emp + " cheque: " +id_cheque);
      $("#resultados_gastos").html("<div class='d-flex justify-content-center'>\
  <div class='spinner-border' role='status'>\
    <span class='sr-only'>Loading...</span>\
  </div>\
</div>");
  $("#resultados_gastos").css("display","block");

          $.ajax({
            url: "funciones/ReporteGastosMensual.php",
            type: "POST",
            // dataType: "json",
            data: {"empresa":empresa,"ejercicio":ejercicio,"periodo":periodo,"estatus":1,"tipo":tipo},
            success: function(data) {
              //Si no encuentra ningun resultado
              // console.log(data);
              TraerResultadosReporteGastos_New();
            }
          });
});

function TraerResultadosReporteGastos_New(){

  var departamento = $("#departamento_gastos_new").val();
  var sucursal = $("#sucursal_gastos_new").val();
  var ejercicio = $("#ejercicio_gastos_new").val();
  var empresa = $("#empresa_gastos_new").val();
  var periodo_num =  $("#periodo_gastos_new").val();
  var periodo = $("#periodo_gastos_new option:selected").text();
  var sucursal_nombre = $("#sucursal_gastos_new option:selected").text();
  var departamento_nombre = $("#departamento_gastos_new option:selected").text();
  var tipo = $('input:radio[name=customRadioInline1_new]:checked').val();



  $.ajax({
      url: "funciones/ReporteGastosMensualTablaNew.php",
      type: "POST",
      // dataType: "json",
      data: {"departamento":departamento,"sucursal":sucursal,"ejercicio":ejercicio,"empresa":empresa,"periodo":periodo,"periodo_num":periodo_num,"sucursal_nombre":sucursal_nombre,"departamento_nombre":departamento_nombre,"tipo":tipo},
      success: function(data) {
        //Si no encuentra ningun resultado
        // console.log(data);
        $("#resultados_gastos").html('');
        $("#resultados_gastos").html(data);
        // $("#resultados_gastos").css("display","block");

      }
  });

}


    // -------------------------------------- TERMINA REPORTE DE RELACION DE GASTOS MENSUAL

    function reloadPage(){
        location.reload(true);
    }
    
</script>


</html>