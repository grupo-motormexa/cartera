<?php
session_start();
$usuario = $_SESSION['usuario'];
?>
<div class="card">
    <div class="card-header">
        <h5 class="card-title">Reporte Cartera</h5>
    </div>
    <div class="card-body">
        <p>
            <button id="abrir_filtrador" class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-plus-square"></i> Filtrar
            </button>
        </p>
        <div class="collapse" id="collapseExample">
            <div class="card card-body">
                <form action="" id="Form_reporte_cartera" class="form-horizontal" onsubmit=" return false">
                    <input id="id_user" type="hidden" name="id_user" value="<?php echo $usuario; ?>">
                    <div class="form-inline">
                        <div class="form-group mb-2">
                            <label for="reporte_cartera_emp">
                                Empresa
                            </label>
                            <select class="form-control inputstyle" id="reporte_cartera_emp" name="reporte_cartera_emp">
                                <!-- <option selected="" value="0">Selecciona una opción</option> -->
                                <?php
                                if ($usuario == 'lagos') {
                                    echo "<option value='01'>Grupo Motormexa</option>";
                                } else {
                                    echo "<option value='00'>Todas las empresas</option>\
                        <option value='01'>Grupo Motormexa</option>\
                        <option value='02'>Automotriz Motormexa</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <!-- <div class="form-group mb-2">
                            <label for="reporte_cartera_ano">
                                Año
                            </label>
                            <select class="form-control inputstyle" id="reporte_cartera_ano" name="reporte_cartera_ano">
                                <option value="2023">2023</option>
                                <option value="2022">2022</option>
                                <option value="2021">2021</option>
                                <option value="2020">2020</option>
                            </select>
                        </div> -->
                        <div class="form-group mb-2">
                            <!-- <label for="reporte_cartera_fecha">
                                Mes
                            </label> -->
                            <!-- <div class="form-group col-md-2"> -->
                            <label for="reporte_cartera_fecha">Fecha</label>
                            <input class="form-control datepicker inputstyle" data-date-format="yyyy-mm-dd" id="reporte_cartera_fecha" autocomplete="off" required>
                            <!-- <select class="form-control inputstyle" id="reporte_cartera_fecha" name="reporte_cartera_fecha">
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Septiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                            </select> -->
                        </div>
                        <div class="form-group mb-2">
                            <label for="id_sucursal">Sucursal</label>
                            <select class="form-control inputstyle" id="id_sucursal">
                                <?php if ($usuario == 'lagos') {
                                    echo "<option value='Lagos'>LAGOS</option>";
                                } else {
                                    echo "<option value='00'>TODAS</option>\
                            <option value='Vallarta'>VALLARTA</option>\
                            <option value='Lagos'>LAGOS</option>\
                            <option value='Acueducto'>ACUEDUCTO</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group mb-2">
                            <label for="id_clasificacion">Clasificación</label>
                            <select class="form-control inputstyle" id="id_clasificacion">
                                <?php if ($usuario == 'lagos') {
                                    echo "<option value='00'>TODOS</option>";
                                } else {
                                    echo "<option value='00'>TODOS</option>\
                            <option value='CASA CUERVO'>CASA CUERVO</option>\
                            <option value='ASEGURADORAS'>ASEGURADORAS</option>\
                            <option value='DEPENDENCIAS DE GOBIERNO'>DEPENDENCIAS GOBIERNO</option>\
                            <option value='OTROS'>OTROS</option>\
                            <option value='EMPLEADOS'>EMPLEADOS</option>\
                            <option value='CLIENTES'>CLIENTES</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <input id="Boton_buscar_adicionales" class="btn btn-sm btn-success" type="submit" value="Buscar">
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div id="resultado_cartera_general"></div>
        <br><br>
        <div id="div_tabla_reportes_activos_cxc" class="table-responsive">
            <table id="tabla_reportes_activos_cxc" class="table row-border order-column table-sm striped table-sm" style="width: 100%; font-size:12px">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Estatus</th>
                        <th>Usuario</th>
                        <th>Empresa</th>
                        <th>Dia</th>
                        <th>Fecha Solicitado</th>
                        <th>Fecha Generado</th>
                        <th>Archivo</th>
                        <!-- <th>Prestamos</th>
                    <th>Intereses</th>
                    <th>Cheque Prestamo</th>
                    <th>Saldo</th>
                    <th>Cheque</th>
                    <th>Retiro</th>
                    <th>Saldo Final</th> -->
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tfoot>
            </table>
        </div>
    </div>
</div>




<script type="text/javascript">
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            language: 'es',
            endDate: new Date(),
            startDate: new Date('2023-01-01')
        });

        function ValidarReportesActivos() {


            var table = $('#tabla_reportes_activos_cxc').DataTable({
                destroy: true,
                paging: false,
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false,
                ajax: {
                    "url": "funciones/ConsultaReportesGeneradosCxc.php?modo=Consulta&tipo=1",
                    "dataSrc": ""
                },
                columns: [{
                        "data": "id"
                    },
                    {
                        "data": "estatus"
                    },
                    {
                        "data": "usuario"
                    },
                    {
                        "data": "empresa"
                    },
                    {
                        "data": "mes"
                    },
                    {
                        "data": "fecha_solicitado"
                    },
                    {
                        "data": "fecha_generado"
                    },
                    {
                        "data": "url_archivo"
                    },
                ],
                columnDefs: [
                    // {
                    //     targets: [3,4,5,6,7,8,9,10,11],
                    //     className: 'dt-body-right'
                    // }
                ],

                language: idioma_espanol,
                order: [
                    [0, "desc"]
                ]
            });


        }
        setInterval(ValidarReportesActivos, 3000);

        ValidarReportesActivos();

    });

    $('#Form_reporte_cartera').on('submit', function(e) {
        var empresa = $("#reporte_cartera_emp").val();
        var fecha = $("#reporte_cartera_fecha").val();
        // var anio = $("#reporte_cartera_ano").val();
        var clasificacion = $("#id_clasificacion").val();
        var sucursal = $("#id_sucursal").val();
        var empresa_nombre =  $( "#reporte_cartera_emp option:selected" ).text();

        $("#resultado_cartera_general").html("<div class='d-flex justify-content-center'>\
        <div class='spinner-border' role='status'>\
            <span class='sr-only'>Loading...</span>\
        </div>\
        </div>");

        $.ajax({
            url: "../funciones/EjecutarReporteGeneralCartera.php",
            // url: "../funciones/ReporteGeneralCartera.php",
            type: "POST",
            // dataType: "JSON",
            data: {
                "fecha": fecha,
                "empresa": empresa,
                "clasificacion": clasificacion,
                "sucursal": sucursal,
                "empresa_nombre": empresa_nombre,
            },
            success: function(datas) {
                // $("#boton_descarga_cartera").html("<a class='btn btn-success' href='../docs/Servicio/Servicio_"+fecha+".xlsx' download='Servicio_"+fecha+".xlsx' role='button'>Descargar</a>");

                $("#resultado_cartera_general").html(datas);


            }
        });

        return false;
    });
</script>