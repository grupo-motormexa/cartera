<div class="container-fluid pt-3">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Busqueda Notas de Credito en Cartera</h5>
                </div>
                <div class="card-body">
                    <form id="busqueda_notas_credito" class="form-inline" action="" onsubmit="return false">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="factura_nota">Factura</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="factura_nota" placeholder="Factura">
                            </div>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Buscar">                       
                    </form>
                    <small>Ingresa parte de la factura para buscar</small>
                    <br>
                    <div id="div_tabla_nota_credito" class="table-responsive" style="display: none;">
                    <br>
                        <table id="tabla_nota_credito" class="table table-sm table-striped" style="width: 100%;">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Id</th>
                                    <th>Factura</th>
                                    <th>Empresa</th>
                                    <th>Cliente</th>
                                    <th>RFC</th>
                                    <th>Nota Credito</th>
                                    <th>Fecha Nota</th>
                                    <th>Importe Nota</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div id="div_tabla_nota_credito_error" style="display: none;">
                        <p>No se encuentra la factura intenta con otra</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
$(document).ready(function() {

    $("#tabla_nota_credito").find("tr:gt(0)").remove();

});

$('#busqueda_notas_credito').on('submit', function(e){
// $(document).on('submit', '#busqueda_notas_credito', function() {

    var factura = $("#factura_nota").val();
    $("#tabla_nota_credito").find("tr:gt(0)").remove();

    $.ajax({
        url: "funciones/Busqueda_notas_credito.php",
        type: "POST",
        data: {
            "factura": factura,
        },
        success: function(datas) {
            // console.log(datas.length);
            if(datas.length > 0){
                $("#div_tabla_nota_credito_error").css("display","none");
                $("#div_tabla_nota_credito").css("display","block");

                $.each(datas, function(i, item) {
                var $tr = $('<tr>').append(
                    $('<td>').text(item.id),
                    $('<td>').text(item.factura),
                    $('<td>').text(item.empresa),
                    $('<td>').text(item.cliente),
                    $('<td>').text(item.rfc),
                    $('<td>').text(item.nota_credito),
                    $('<td>').text(item.fecha),
                    $('<td>').text(item.importe),
                ).appendTo('#tabla_nota_credito tbody');
                // console.log($tr.wrap('<p>').html());
            });

            $("#tabla_nota_credito").DataTable({
                "autoWidth": true,
                "columnDefs": [
                    
                        { type: 'currency', targets: 7 },
                    
                ],
                "language": idioma_espanol,
                "ordering": true,
                "searching": true,
                "paging": true,
                "info": true
            });
            }else{
                $("#div_tabla_nota_credito_error").css("display","block");
                $("#div_tabla_nota_credito").css("display","none");
            }


        }
    });

    return false;

});






var idioma_espanol = {
    "decimal": "",
    "emptyTable": "No hay datos disponibles",
    "info": "Viendo _START_ a _END_ de _TOTAL_ registros",
    "infoEmpty": "Viendo 0 a 0 de 0 registros",
    "infoFiltered": "(Filtrado de _MAX_ registros totales)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "Ver _MENU_ registros",
    "loadingRecords": "Cargando...",
    "processing": "Procensando...",
    "search": "Buscar:",
    "zeroRecords": "No se encontraron registros",
    "paginate": {
        "first": "Primero",
        "last": "Ultimo",
        "next": "Siguiente",
        "previous": "Anterior"
    },
    "aria": {
        "sortAscending": ": activate to sort column ascending",
        "sortDescending": ": activate to sort column descending"
    }
}
</script>