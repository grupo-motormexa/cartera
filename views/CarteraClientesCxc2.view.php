<div class="card" style='font-size:12px'>
    <div class="card-header">
        <h5 class="card-title">Reporte de Cartera</h5>
    </div>
    <div class="card-body">
        <p>

            <?php
            session_start();
            $usuario = $_SESSION['usuario'];

            function ConexionCartera2024()
            {
                try {
                    $conexion = new mysqli('200.1.1.11', 'soporte', 'chanel', 'cartera');
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }
                return $conexion;
            }

            $conexion = ConexionCartera2024();
            $conexion->set_charset('utf8');

            $statement = $conexion->prepare("SELECT * FROM `notificaciones`");
            $statement->execute();
            $resultados = $statement->get_result();

            while ($fila = $resultados->fetch_assoc()) {
                if ($usuario == $fila['usuario'] && $fila['estatus'] == 1) {
                    echo "<div id='notificaciones_div_" . $fila['id'] . "' class='alert " . $fila['tipo'] . " alert-dismissible fade show' role='alert'>
                    <strong id='titulo_notifi'>" . $fila['encabezado'] . "</strong><span id='mensaje_notifi'> " . $fila['comentario'] . "</span>
                    <button onclick='CerrarNotificaciones(" . $fila['id'] . ");' type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                </div>";
                }
            }
            ?>
            <input id="id_user" type="hidden" name="id_user" value="<?php echo $usuario; ?>">

            <button id="abrir_filtrador" class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-plus-square"></i> Filtrar
            </button>
        </p>
        <div class="collapse" id="collapseExample">
            <div class="card card-body">

                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="id_clasificacion">Clasificación</label>
                        <select class="form-control form-control-sm chosen-select" id="id_clasificacion" data-placeholder=" " multiple style="width:100%;">
                            <option value=""></option>
                            <option value="1">Casa Cuervo</option>
                            <option value="2">Aseguradoras</option>
                            <option value="3">Dependencias de Gobierno</option>
                            <option value="4">Otros</option>
                            <option value="5">Empleados</option>
                            <option value="6">Clientes</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="id_tipo_cartera">Estatus Cartera</label>
                        <select class="form-control form-control-sm" id="id_tipo_cartera">
                            <option value=""></option>
                            <option value="Vencida">Vencida</option>
                            <option value="Vigente">Por vencer</option>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="id_empresa">Empresa</label>
                        <select class="form-control form-control-sm" id="id_empresa">
                            <option value=""></option>
                            <option value="01">Grupo Motormexa</option>
                            <option value="02">Automotriz Motormexa</option>
                        </select>
                    </div>
                    <div class="form-group col-md-1">
                        <label for="id_sucursal">Sucursal</label>
                        <select class="form-control form-control-sm" id="id_sucursal">
                            <?php if ($usuario == 'lagos') {
                                echo "<option value='L'>Lagos</option>";
                            } else {
                                echo "<option value=''></option>\
                            <option value='V'>Vallarta</option>\
                            <option value='L'>Lagos</option>\
                            <option value='A'>Acueducto</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-1">
                        <label for="id_tipo">Tipo</label>
                        <select class="form-control form-control-sm chosen-select" id="id_tipo" data-placeholder=" " multiple style="width:100%;">
                            <option value=""></option>
                            <?php

                            $statement = $conexion->prepare("SELECT departamento FROM `filtros_usuarios` WHERE id_usuario = '$usuario'");
                            $statement->execute();
                            $resultados = $statement->get_result();

                            while ($fila = $resultados->fetch_assoc()) {
                                $carteras = explode(",",$fila['departamento']);

                                foreach($carteras as $departamento){
                                    if($departamento == 'Servicio'){
                                        echo "<option value='".$departamento[0]."'>".$departamento."</option>\
                                        <option value='G'>Garantia</option>";
                                    }else{
                                        echo "<option value='".$departamento[0]."'>".$departamento."</option>";
                                    }
                                    
                                }
                                // if ($usuario == $fila['usuario']) {
        //                             echo "<div id='notificaciones_div_" . $fila['id'] . "' class='alert " . $fila['tipo'] . " alert-dismissible fade show' role='alert'>
        // <strong id='titulo_notifi'>" . $fila['encabezado'] . "</strong><span id='mensaje_notifi'> " . $fila['comentario'] . "</span>
        // <button onclick='CerrarNotificaciones(" . $fila['id'] . ");' type='button' class='close' data-dismiss='alert' aria-label='Close'>
        //     <span aria-hidden='true'>&times;</span>
        // </button>
    // </div>";
                                // }
                            }
                            ?>
<!-- 
                            <option value="R">Refacciones</option>
                            <option value="S">Servicio</option>
                            <option value="B">Body</option>
                            <option value="G">Garantia</option> -->
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="id_vendedor">Vendedor</label>
                        <select class="form-control form-control-sm" id="id_vendedor">

                        </select>
                    </div>
                    <div class="form-group col-md-1">
                        <label for="factura_bbj">Factura BBJ</label>
                        <input type="text" class="form-control form-control-sm" id="factura_bbj">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="id_cliente">Cliente</label>
                        <select class="form-control form-control-sm chosen-select" id="id_cliente" data-placeholder=" " multiple style="width:100%;">

                        </select>
                    </div>
                    <div class="form-group col-md-5">
                        <label for="id_factura_inicial">Rango de facturas</label>
                        <div class="form-inline">
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">De la: </label>
                            <input type="number" class="form-control form-control-sm mx-sm-3 float-right" name="id_factura_inicial" id="id_factura_inicial">
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">A la: </label>
                            <input type="number" class="form-control form-control-sm mx-sm-3 float-right" name="id_factura_final" id="id_factura_final">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="id_estatus">Estatus</label>
                        <select class="form-control form-control-sm chosen-select" id="id_estatus" data-placeholder=" " multiple style="width:100%;" tabindex="4">

                        </select>
                    </div>
                    <div class="form-group col-md-1">
                        <label for="id_pedido">Pedido</label>
                        <input type="text" class="form-control form-control-sm" id="id_pedido">
                    </div>
                    <div class="form-group col-md-1">
                        <label for="id_factura">Factura</label>
                        <input type="text" class="form-control form-control-sm" id="id_factura">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="fecha_inicial">Rango de fechas</label>
                        <div class="form-inline">
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">De: </label>
                            <input type="text" class="form-control form-control-sm mx-sm-3 float-right datepicker" data-date-format="yyyy-mm-dd" name="fecha_inicial" id="fecha_inicial" autocomplete="off">
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Hasta: </label>
                            <input type="text" class="form-control form-control-sm mx-sm-3 float-right datepicker" data-date-format="yyyy-mm-dd" name="fecha_final" id="fecha_final" autocomplete="off">
                        </div>
                    </div>
                    <!-- <div class="form-group col-md-6">

        </div> -->
                    <div class="form-group col-md-1 float-right">
                        <label for="boton_limpiar_cartera">&nbsp;</label>
                        <input class="btn btn-secondary btn-sm form-control form-control-sm" type="button" value="Limpiar" id="boton_limpiar_cartera">
                    </div>
                    <div class="form-group col-md-1 float-right">
                        <label for="boton_filtrar_cartera">&nbsp;</label>
                        <input class="btn btn-success btn-sm form-control form-control-sm" type="button" value="Buscar" id="boton_filtrar_cartera">
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div id="div_tabla_cartera" class="table-responsive" style="display: none;">
            <table class="table table-sm" id="tabla_cartera" style='width:100%; font-size:11px'>
                <thead class="thead-dark">
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Empresa</th>
                        <th>Vendedor</th>
                        <th>Fecha</th>
                        <th>Importe</th>
                        <th>Abonos</th>
                        <th>Nota Credito</th>
                        <th>Saldo</th>
                        <th>Factura</th>
                        <th>Factura BBJ</th>
                        <th>Cliente</th>
                        <th>Fecha Pago</th>
                        <th>Fecha Portal</th>
                        <th>Contra Recibo</th>
                        <th>Estatus</th>
                        <th>Comentario</th>
                        <th>Zona</th>
                        <th>Pedidos</th>
                        <th>Complementos</th>
                        <th>Fecha Aprox Pago</th>
                        <th>Clasificación</th>
                        <th>Tipo Cartera</th>
                        <th>Siniestro</th>
                        <th>Pedido(s)</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <br>
            <br>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_cartera" tabindex="-1" aria-labelledby="modal_cartera" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Seguimiento Cobranza</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_cartera">
                <form action="" class="form-horizontal" id='form_movimientos_cartera' onsubmit='return false'>
                    <div class="form-row">
                        <div class="col-md-12 form-group">
                            <label for="cuenta_contable">Cuenta contable</label>
                            <input type="text" class="form-control form-control-sm" id="cuenta_contable" disabled="disabled">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="estatus_actual">Estatus</label>
                            <select class="form-control form-control-sm" id="estatus_actual" selected="true" disabled="disabled">
                            </select>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="fecha_recibida">Fecha Recibida</label>
                            <input type="text" class="form-control form-control-sm datepicker" data-date-format="yyyy-mm-dd" name="fecha_recibida" id="fecha_recibida" autocomplete="off">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="fecha_portal">Fecha Portal o contrarecibo</label>
                            <input type="text" class="form-control form-control-sm datepicker" data-date-format="yyyy-mm-dd" name="fecha_portal" id="fecha_portal" autocomplete="off">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="numero_contrarecibo">Contra Recibo</label>
                            <input type="text" class="form-control form-control-sm" name="numero_contrarecibo" id="numero_contrarecibo" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="fecha_devuelta">Fecha Devuelta</label>
                            <input type="text" class="form-control form-control-sm datepicker" data-date-format="yyyy-mm-dd" name="fecha_devuelta" id="fecha_devuelta" autocomplete="off">
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="fecha_aprox_pago">Fecha Aproximada Pago</label>
                            <input type="text" class="form-control form-control-sm datepicker" data-date-format="yyyy-mm-dd" name="fecha_aprox_pago" id="fecha_aprox_pago" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="fecha_pago">Fecha Pago</label>
                            <input type="text" class="form-control form-control-sm datepicker" data-date-format="yyyy-mm-dd" name="fecha_pago" id="fecha_pago" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="zona_factura">Zona</label>
                            <input type="text" class="form-control form-control-sm" name="zona_factura" id="zona_factura" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="estatus_especial">Estatus Especial</label>
                            <select class="form-control form-control-sm" id="estatus_especial">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>
                <button id='Boton_guardar_cartera' onclick='Guardar_cartera();' type='button' class='btn btn-primary'>Guardar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_cartera_seguimiento" tabindex="-1" aria-labelledby="modal_cartera" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizaciones Cartera</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id_cartera_seguimiento">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="input-group">
                            <textarea tabindex="1" class="form-control custom-control" id="modal_comentario_cartera" rows="2" data-toggle="tooltip" title="Es necesario ingresar la actualización" data-delay='{"show":"0", "hide":"300"}'></textarea>
                            <span class="input-group-addon btn btn-success input-group-text" tabindex="2" onclick="Guardar_seguimiento_cartera();">Guardar</span>
                        </div>
                    </div>
                    <div class="col-md-12" id="tabla_historia_cartera">
                        <table id="tabla_actualizaciones_cartera" class="table table-sm" style="width:100%; font-size:10px">
                            <thead>
                                <tr>
                                    <th style="width: 20%">Fecha</th>
                                    <th style="width: 10%">Usuario</th>
                                    <th>Comentario</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>
                <!-- <button id='Boton_guardar_cartera' onclick='Guardar_cartera();' type='button' class='btn btn-primary'>Guardar</button> -->
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {

        var usuario = $("#id_user").val();

        $(".chosen-select").chosen();

        $(".datepicker").datepicker({
            todayBtn: 1,
            autoclose: true,
        });

        $("#fecha_inicial").datepicker({
            todayBtn: 1,
            autoclose: true,
        }).on('changeDate', function(selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#fecha_final').datepicker('setStartDate', minDate);
        });

        $("#fecha_final").datepicker()
            .on('changeDate', function(selected) {
                var maxDate = new Date(selected.date.valueOf());
                $('#fecha_inicial').datepicker('setEndDate', maxDate);
            });

        if (usuario == 'lagos') {
            cargartabla('', '', '', '', 'L', '', '', '', '', '', '', '2024-01-01', '', '', '');
        } else {
            cargartabla('', '', '', '', '', '', '', '', '', '', '', '2024-01-01', '', '', '');
        }

        TraerEstatus();
        TraerVendedores();
        TraerCliente();
    });

    function CerrarNotificaciones(id) {

        $.ajax({
            url: "funciones/CerrarNotificacionesCartera.php",
            type: "POST",
            data: {
                "id": id
            },
            success: function(datas) {

                $("notificaciones_div_" + id).remove();

            }
        });
    }


    function format(d) {
        // `d` is the original data object for the row
        return '<div class="slider">' +
            '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            d.pedidos +
            '</table><br>' +
            '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
            d.complementos +
            '</table>' +
            '</div>';
    }


    function LimpiarFormulario() {
        $("#fecha_recibida").val('');
        $("#fecha_portal").val('');
        $("#fecha_devuelta").val('');
        $("#fecha_aprox_pago").val('');
        $("#fecha_pago").val('');
        $("#numero_contrarecibo").val('');
        $("#zona_factura").val();
        $("#estatus_especial").val();
    }


    $('#boton_limpiar_cartera').on('click', function(e) {
        $("#id_empresa").val('');
        $("#id_sucursal").val('');
        $("#id_tipo").val('');
        $("#id_clasificacion").val('');
        $("#id_vendedor").val('');
        $("#id_cliente").val('');
        $("#id_factura_inicial").val('');
        $("#id_factura_final").val('');
        $("#id_estatus").val('');
        $("#fecha_inicial").val('');
        $("#fecha_final").val('');
        $("#id_pedido").val('');
        $("#id_factura").val('');

    });

    $('#boton_filtrar_cartera').on('click', function(e) {
        id_clasificacion = $("#id_clasificacion").val();
        id_tipo_cartera = $("#id_tipo_cartera").val();
        factura_bbj = $("#factura_bbj").val();
        empresa = $("#id_empresa").val();
        sucursal = $("#id_sucursal").val();
        tipo = $("#id_tipo").val();
        vendedor = $("#id_vendedor").val();
        cliente = $("#id_cliente").val();
        factura_inicial = $("#id_factura_inicial").val();
        factura_final = $("#id_factura_final").val();
        estatus = $("#id_estatus").val();
        fecha_inicial = $("#fecha_inicial").val();
        fecha_final = $("#fecha_final").val();
        pedido = $("#id_pedido").val();
        factura = $("#id_factura").val();
        $("#div_tabla_cartera").css("display", "none");
        if (id_clasificacion == '' && id_tipo_cartera == '' && fecha_inicial == '' && empresa == '' && sucursal == '' && tipo == '' && vendedor == '' && cliente == '' && estatus == '' && fecha_final == '' && pedido == '' && factura == '' && factura_inicial == '' && factura_final == '') {
            fecha_inicial = '2024-01-01'
        } else if (id_clasificacion == '' && id_tipo_cartera == '' && fecha_inicial == '' && empresa == '' && sucursal == '' && tipo == '' && vendedor == '' && cliente == '' && estatus == '4' && fecha_final == '' && pedido == '' && factura == '' && factura_inicial == '' && factura_final == '') {
            fecha_inicial = '2024-01-01'
        }
        cargartabla(factura_bbj, id_clasificacion, id_tipo_cartera, empresa, sucursal, tipo, vendedor, cliente, estatus, pedido, factura, fecha_inicial, fecha_final, factura_inicial, factura_final);

        $("#abrir_filtrador").trigger('click');

    });

    function TraerEstatus() {

        $.ajax({
            url: "funciones/TraerEstatusCarteras.php",
            success: function(datas) {
                $.each(datas, function(key, registro) {
                    if (registro.id >= 1000) {
                        $("#estatus_especial").append('<option value=' + registro.id + '>' + registro.descripcion + '</option>');
                    } 
                        $("#id_estatus").append('<option value=' + registro.id + '>' + registro.descripcion + '</option>');
                    // }
                    if (registro.estatus == 'Activo') {
                        $("#estatus_actual").append('<option value=' + registro.id + '>' + registro.descripcion + '</option>');
                    }
                    // estatus_actual
                });
                $("#id_estatus").val([1, 2, 3, 6, 7, 8, 1000, 1001]).trigger("chosen:updated");
            }
        });
    }


    function TraerCliente() {

        $.ajax({
            url: "funciones/TraerClientesCarteras.php",
            success: function(data) {
                var arrayLength = data.length;
                for (var i = 0; i < arrayLength; i++) {
                    $("#id_cliente").append("<option value='" + data[i]["id"] + "'>" + data[i]["descripcion"] + "</option>");
                }

                $("#id_cliente").trigger("chosen:updated");
                // $("#id_descuentos_monetario").trigger("chosen:updated");   
            }
        });

    }

    function TraerVendedores() {

        $.ajax({
            url: "funciones/TraerVendedorCartera.php",
            success: function(datas) {
                $.each(datas, function(key, registro) {
                    $("#id_vendedor").append('<option value=' + registro.id + '>' + registro.descripcion + '</option>');
                });
                // $("#id_descuentos_monetario").trigger("chosen:updated");   
            }
        });
    }

    function TraerLosSeguimientos(id_factura) {

        $("#tabla_actualizaciones_cartera").DataTable({
            "destroy": true,
            "autoWidth": true,
            "ajax": {
                "dataSrc": "",
                "url": "funciones/SeguimientosCartera.php?id_factura=" + id_factura,
            },
            "columns": [{
                    "data": "fecha"
                },
                {
                    "data": "usuario"
                },
                {
                    "data": "comentario"
                },
            ],
            "language": idioma_espanol,
            "ordering": false,
            "searching": false,
            "paging": false,
            "info": false
        });

    }


    function Guardar_seguimiento_cartera() {
        var id_valor = $("#id_cartera_seguimiento").val();
        var comentario = $("#modal_comentario_cartera").val();
        // var contra_recibo=$

        if (comentario != "") {

            $.ajax({
                url: "funciones/CRUD_cartera.php",
                type: "POST",
                data: {
                    "id": id_valor,
                    "comentario": comentario,
                    "modo": "Alta"
                },
                success: function(data) {
                    $("#modal_comentario_cartera").val("");
                    TraerLosSeguimientos(id_valor);
                    //AQUICERRAMOS

                    $('#modal_cartera_seguimiento').modal('hide');

                    Swal.fire({
                        position: 'top',
                        icon: 'success',
                        title: 'Actualizado',
                        text: 'Comentario agregado de manera exitosa !',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            });

        } else {
            $("[data-toggle='tooltip']").tooltip('toggle');
        }

    }


    function cargartabla(factura_bbj, id_clasificacion, id_tipo_cartera, empresa, sucursal, tipo, vendedor, cliente, estatus, pedido, factura, fecha_inicial, fecha_final, factura_inicial, factura_final) {

        $('#tabla_cartera tbody').off('click');
        var table = $("#tabla_cartera").DataTable({
            "destroy": true,
            "autoWidth": false,
            "rowId": 'id',
            "paging": true,
            "ajax": {
                // "method":"POST",
                "dataSrc": "",
                "url": "funciones/ReportedeCartera3.php?factura_bbj=" + factura_bbj + "&id_clasificacion=" + id_clasificacion + "&id_tipo_cartera=" + id_tipo_cartera + "&empresa=" + empresa + "&sucursal=" + sucursal + "&tipo=" + tipo + "&vendedor=" + vendedor + "&cliente=" + cliente + "&estatus=" + estatus + "&pedido=" + pedido + "&factura=" + factura + "&fecha_inicial=" + fecha_inicial + "&fecha_final=" + fecha_final + "&factura_inicial=" + factura_inicial + "&factura_final=" + factura_final,
            },
            "columns": [{
                    "className": 'dt-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                    "data": "id"
                },
                {
                    "data": "empresa"
                },
                {
                    "data": "vendedor"
                },
                {
                    "data": "fecha"
                },
                {
                    "data": "importe_total",
                    render: $.fn.dataTable.render.number(',', '.', 2)
                },
                {
                    "data": "abono",
                    render: $.fn.dataTable.render.number(',', '.', 2)
                },
                {
                    "data": "nota_credito",
                    render: $.fn.dataTable.render.number(',', '.', 2)
                },
                {
                    "data": "saldo",
                    render: $.fn.dataTable.render.number(',', '.', 2)
                },
                {
                    "data": "factura"
                },
                {
                    "data": "factura_bbj"
                },
                {
                    "data": "cliente"
                },
                {
                    "data": "fecha_pago"
                },
                {
                    "data": "fecha_portal"
                },
                {
                    "data": "contra_recibo"
                },
                {
                    "data": "estatus"
                },
                {
                    "data": "comentario"
                },
                {
                    "data": "zona"
                },
                {
                    "data": "pedidos"
                },
                {
                    "data": "complementos"
                },
                {
                    "data": "fecha_aprox_pago"
                },
                {
                    "data": "id_clasificacion"
                },
                {
                    "data": "id_tipo_cartera"
                },
                {
                    "data": "siniestro"
                },
                {
                    "data": "pedido"
                },
                {
                    "data": "opciones"
                },
                //   {
                //     render: function(data, type, row) {
                //         return "<button type='button' class='btn btn-success btn_small btn-sm ver'><i class='fas fa-eye'></i></i></button>";
                //     }
                //   }
            ],
            "footerCallback": function(row, data, start, end, display) {
                var api = this.api();
                // nb_cols = api.columns().nodes().length;
                var j = 5;
                $(api.column(4).footer()).html('TOTAL');

                while (j < 9) {

                    // var pageTotal = api
                    // .column( j, { page: 'current'} )
                    // .data()
                    // .reduce( function (a, b) {
                    //     return Number(a) + Number(b);
                    // }, 0 );

                    var Total = api
                        .column(j)
                        .data()
                        .reduce(function(a, b) {
                            return Number(a) + Number(b);
                        }, 0);

                    // Update footer

                    // $( api.column( 5 ).footer() ).html(formatCurrency("en-US", "USD", 2, pageTotal));
                    // $( api.column( 6 ).footer() ).html('TOTAL GENERAL');
                    $(api.column(j).footer()).html(formatCurrency("en-US", "USD", 2, Total));
                    // $( api.column( j ).footer() ).html('Viendo: '+formatCurrency("en-US", "USD", 2, pageTotal)+'<br> Total: '+formatCurrency("en-US", "USD", 2, Total));
                    j++;
                }

                // $( api.column( 3 ).footer() ).addClass('good');
                // $( api.column( 4 ).footer() ).addClass('good');
                // $( api.column( 5 ).footer() ).addClass('wrong');
                // $( api.column( 10 ).footer() ).addClass('wrong');

            },
            "columnDefs": [{
                targets: [13, 16, 17, 18, 19, 20, 21, 22, 23, 24],
                "visible": false
            }],
            "language": idioma_espanol,
            "order": [
                [4, "desc"]
            ],
            'pageLength': 10,
            "lengthMenu": [
                [10, 25, 50, 100],
                [10, 25, 50, 100]
            ],
            "paging": true,
            "dom": 'Bfrtip',
            // "dom": '<"dt-buttons"Bf><"clear">lirtp',
            "buttons": [{
                    extend: 'excelHtml5',
                    text: 'Excel',
                    filename: 'reporte_cartera',
                    title: '',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 16, 23, 24, 20, 21]
                    }
                },
                // {
                //     extend : 'print', 
                //     action : function( ) {
                //         dt_print()
                //     }
                // }
            ]
        });

        window.onresize = function() {
            var w = this.innerWidth;
            table.column(6).visible(w > 1600);
            table.column(3).visible(w > 1500);
            // table.column(1).visible( w > 1400);
            table.column(10).visible(w > 1300);
            table.column(4).visible(w > 1200);
        }

        $(window).trigger('resize');

        $('#tabla_cartera tbody').on('click', 'td.dt-control', function() {

            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });



        $("#div_tabla_cartera").css("display", "block");

        $('#tabla_cartera tbody').on('click', '.ver', function() {
            LimpiarFormulario();
            var id = $(this).parents("tr").find("td:eq(1)").html();

            $("#id_cartera").val(id);

            $.ajax({
                url: "funciones/InformacionCartera.php",
                type: "POST",
                data: {
                    "id": id
                },
                datatype: 'json',
                success: function(datas) {
                    // console.log(datas.estatus);
                    if (datas.estatus == 4 || datas.estatus == 5) {
                        $("#estatus_actual").attr("disabled", true);
                        $("#fecha_portal").attr("disabled", true);
                        $("#fecha_devuelta").attr("disabled", true);
                        $("#fecha_aprox_pago").attr("disabled", true);
                        $("#fecha_pago").attr("disabled", true);
                        $("#fecha_recibida").attr("disabled", true);
                        $("#Boton_guardar_cartera").attr("disabled", true);

                    } else if (datas.estatus == 2) {
                        // $("#fecha_recibida").attr("disabled", true);
                        $("#fecha_portal").attr("disabled", false);
                        $("#fecha_devuelta").attr("disabled", false);
                        $("#fecha_aprox_pago").attr("disabled", false);
                        $("#fecha_pago").attr("disabled", false);
                        $("#Boton_guardar_cartera").attr("disabled", false);
                    } else if (datas.estatus == 3) {
                        // $("#fecha_recibida").attr("disabled", true);
                        // $("#fecha_portal").attr("disabled", true);
                        // $("#fecha_devuelta").attr("disabled", true);
                        $("#fecha_aprox_pago").attr("disabled", false);
                        $("#fecha_pago").attr("disabled", false);
                        $("#Boton_guardar_cartera").attr("disabled", false);
                    } else {
                        // $("#estatus_actual").attr("disabled", false);
                        $("#fecha_portal").attr("disabled", false);
                        $("#fecha_devuelta").attr("disabled", false);
                        $("#fecha_aprox_pago").attr("disabled", false);
                        $("#fecha_pago").attr("disabled", false);
                        $("#fecha_recibida").attr("disabled", false);
                        $("#Boton_guardar_cartera").attr("disabled", false);
                    }
                    $("#cuenta_contable").val(datas.cuenta_contable);
                    $("#estatus_actual").val(datas.estatus);
                    $("#fecha_portal").val(datas.fecha_portal);
                    $("#fecha_recibida").val(datas.fecha_recibida);
                    $("#fecha_devuelta").val(datas.fecha_devuelta);
                    $("#fecha_aprox_pago").val(datas.fecha_aprox_pago);
                    $("#fecha_pago").val(datas.fecha_pago);
                    $("#numero_contrarecibo").val(datas.numero_contrarecibo);
                    $("#zona_factura").val(datas.zona_factura);
                    $("#estatus_especial").val(datas.estatus_especial);
                }
            });

            $('#modal_cartera').modal('toggle');

        });

        $('#tabla_cartera tbody').on('click', '.seguimiento', function() {
            var id = $(this).parents("tr").find("td:eq(1)").html();

            $("#id_cartera_seguimiento").val(id);

            TraerLosSeguimientos(id);

            $('#modal_cartera_seguimiento').modal('toggle');
        });

    }

    $('#form_movimientos_cartera input,select').on('change', function() {

        fecha_recibida = $("#fecha_recibida").val();
        fecha_aprox_pago = $("#fecha_aprox_pago").val();
        fecha_pago = $("#fecha_pago").val();
        fecha_devuelta = $("#fecha_devuelta").val();
        fecha_portal = $("#fecha_portal").val();
        estatus_especial = $("#estatus_especial").val();

        if (estatus_especial != "") {
            $("#estatus_actual").val(estatus_especial);
        }else if (fecha_pago == '' && fecha_recibida == '' && fecha_aprox_pago == '' && fecha_devuelta == '' && fecha_portal == '') {
            $("#estatus_actual").val(1);
        } else if (fecha_pago != '') {
            $("#estatus_actual").val(4);
        } else if (fecha_pago == "" && fecha_devuelta != "") {
            $("#estatus_actual").val(6);
        } else if (fecha_pago == "" && fecha_devuelta == "" && fecha_portal != "") {
            $("#estatus_actual").val(3);
        } else if (fecha_pago == "" && fecha_devuelta == "" && fecha_portal == "" && fecha_recibida != "") {
            $("#estatus_actual").val(2);
        }


    });


    function Guardar_cartera() {

        id = $("#id_cartera").val();
        estatus = $("#estatus_actual").val();
        estatus_text = $("#estatus_actual option:selected").text();
        fecha_portal = $("#fecha_portal").val();
        fecha_devuelta = $("#fecha_devuelta").val();
        fecha_aprox_pago = $("#fecha_aprox_pago").val();
        fecha_recibida = $("#fecha_recibida").val();
        fecha_pago = $("#fecha_pago").val();
        contra_recibo = $("#numero_contrarecibo").val();
        zona_factura = $("#zona_factura").val();
        estatus_especial = $("#estatus_especial").val();

        var table2 = $("#tabla_cartera").DataTable();

        var id_tabla = table2.row('[id=' + id + ']').index();
        new_datos = table2.row(id_tabla).data();
        new_datos['estatus'] = estatus_text;

        if (estatus_text == 'Pagada') {
            new_datos['fecha_pago'] = fecha_pago;
            // table2.row(id_tabla).data( new_datos_fecha ).draw();
        }

        table2.row(id_tabla).data(new_datos).draw();

        $.ajax({
            url: "funciones/CRUD_cartera.php",
            type: "POST",
            data: {
                "id": id,
                "estatus": estatus,
                "fecha_recibida": fecha_recibida,
                "fecha_portal": fecha_portal,
                "fecha_devuelta": fecha_devuelta,
                "fecha_aprox_pago": fecha_aprox_pago,
                "fecha_pago": fecha_pago,
                "contra_recibo": contra_recibo,
                "zona_factura": zona_factura,
                "estatus_especial": estatus_especial,
                "modo": "Modificar"
            },
            // datatype: 'json',
            success: function(datas) {

                $('#modal_cartera').modal('hide');

                Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Actualizado',
                    text: 'Esta factura ha sido actualizada !',
                    showConfirmButton: false,
                    timer: 1500
                })

            }
        });

    }


    $("#collapseExample").keyup(function(event) {
        if (event.keyCode == 13) {
            $("#boton_filtrar_cartera").click();
        }
    });


    function formatCurrency(locales, currency, fractionDigits, number) {
        var formatted = new Intl.NumberFormat(locales, {
            style: 'currency',
            currency: currency,
            minimumFractionDigits: fractionDigits
        }).format(number);
        return formatted;
    }
</script>