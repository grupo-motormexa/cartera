<div class="card">
    <div class="card-header">
        <h5 class="card-title">Administración Permisos en Cartera</h5>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Usuario</h5>
                        <select class="form-control form-control-sm chosen-select" data-placeholder=" " style="width:100%;" id="usuarios_permisos_cartera"></select>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Permisos de Editar</h5>
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault" name="flexSwitchCheckDefault">
                            <label class="form-check-label" for="flexSwitchCheckDefault">Puede editar las facturas</label>
                        </div>
                        <br>
                        <hr><br>
                        <h5 class="card-title">Carteras Activas</h5>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Servicio">
                            <label class="form-check-label" for="inlineCheckbox1">Servicio</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="Refacciones">
                            <label class="form-check-label" for="inlineCheckbox2">Refacciones</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="Body">
                            <label class="form-check-label" for="inlineCheckbox3">Body</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="Garantia">
                            <label class="form-check-label" for="inlineCheckbox4">Garantia</label>
                        </div>
                        <br><br><br>
                        <button id="actualizar_permisos_cartera" class="btn btn-success"><i class="fas fa-check"></i> Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="card-body">
        <select class="form-control form-control-sm chosen-select" data-placeholder=" " style="width:100%;" id="usuarios_permisos_cartera">

        </select>
        <br><br>
        <div id="jstree">

        </div>
        <br>
        <div id="Boton_actualizar">

        </div>
    </div> -->
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $(".chosen-select").chosen();

        TraerUsuarios();
        TraerPermisosCartera();
    });

    function TraerUsuarios() {

        select = "#usuarios_permisos_cartera";
        opcion = 1;

        $.ajax({
            url: "funciones/TodosUsuarios.php",
            type: "POST",
            data: {
                "estatus": 'Activo',
                "menu": "Cartera Actualizada"
            },
            success: function(datas) {

                $.each(datas, function(key, value) {
                    $(select).append($('<option>', {
                        value: value.id,
                        text: value.usuario
                    }));
                })

                $(select).val(opcion).trigger("chosen:updated");
            }

        });

    }

    $("#usuarios_permisos_cartera").on('change', function() {

        TraerPermisosCartera();
    });


    function TraerPermisosCartera() {

        // id_usuario = $("#usuarios_permisos_cartera").val();
        id_usuario = $("#usuarios_permisos_cartera option:selected").text();

        // console.log(id_usuario);
        LimpiarFiltrosCartera();

        $.ajax({
            url: "funciones/TraerPermisosCartera.php",
            type: "POST",
            dataType: "JSON",
            data: {
                "id_usuario": id_usuario
            },
            success: function(datas) {

                if (datas[0].permiso == 1) {
                    $("#flexSwitchCheckDefault").attr("checked", true);
                } else {
                    $("#flexSwitchCheckDefault").attr("checked", false);
                }

                if (Array.isArray(datas[0].filtro)) {

                    for (i = 0; i < datas[0].filtro.length; i++) {
                        switch (datas[0].filtro[i]) {
                            case 'Servicio':
                                $("#inlineCheckbox1").attr("checked", true);
                                break;
                            case 'Refacciones':
                                $("#inlineCheckbox2").attr("checked", true);
                                break;
                            case 'Body':
                                $("#inlineCheckbox3").attr("checked", true);
                                break;
                            case 'Garantia':
                                $("#inlineCheckbox4").attr("checked", true);
                                break;
                        }
                    }
                }

            }

        });
    }

    function LimpiarFiltrosCartera() {
        $("#inlineCheckbox1").attr("checked", false);
        $("#inlineCheckbox2").attr("checked", false);
        $("#inlineCheckbox3").attr("checked", false);
        $("#inlineCheckbox4").attr("checked", false);
    }

    $(document).on('click', '#actualizar_permisos_cartera', function() {

        id_usuario = $("#usuarios_permisos_cartera option:selected").text();

        if ($('#flexSwitchCheckDefault').prop('checked')) {
            permiso_edicion = 'SI';
        } else {
            permiso_edicion = 'NO';
        }

        CarterasPermitidas = [];

        if ($('#inlineCheckbox1').prop('checked')) {
            CarterasPermitidas.push("Servicio");
        } 

        if ($('#inlineCheckbox2').prop('checked')) {
            CarterasPermitidas.push("Refacciones");
        } 

        if ($('#inlineCheckbox3').prop('checked')) {
            CarterasPermitidas.push("Body");
        }

        if ($('#inlineCheckbox4').prop('checked')) {
            CarterasPermitidas.push("Garantia");
        }

        // console.log(permiso_edicion + " " + CarterasPermitidas);

        $.ajax({
            url: "funciones/ActualizarPermisosCartera.php",
            type: "POST",
            // dataType: "JSON",
            data: {
                "id_usuario": id_usuario,
                "permisos_cartera": CarterasPermitidas,
                "permiso_edicion" : permiso_edicion
            },
            success: function(datas) {

                Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'Actualizado',
                    text: 'Permisos Actualizados !',
                    showConfirmButton: false,
                    timer: 1500
                });

            }

        });
    });
</script>