<div class="container-fluid pt-3">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Clientes Cartera</h5>
                    </div>
                    <div class="card-body">
                        <!-- <button id="new_vendedores" class="btn btn-success"><i class="fas fa-plus-circle pr-1"></i> Agregar</button> -->
                        <div class="table-responsive">
                            <table id="tabla_catalago_cartera" class="table table-sm table-striped" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Id</th>
                                        <th>Codigo</th>
                                        <th>Codigo Corto</th>
                                        <th>Cliente</th>
                                        <th>RFC</th>
                                        <th>Dias</th>
                                        <th>Agrupación</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- MODAL    -->
    <div class="modal fade" id="modal_catalogo_cartera" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class='modal-title'>Modificar Cliente</h5>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                </div>
                <form action="" id="Form_catalago_cartera" class="form-horizontal" onsubmit=" return false">
                <div class="modal-body">
                        <div class='form-group row'>
                            <label for='modi_codigo_corto' class='col-sm-4 col-form-label'>Codigo Corto</label>
                            <div class='col-sm-8'>
                                <div class='input-group mb-3'>
                                    <div class='input-group-prepend'>
                                        <span class='input-group-text' id='modi_id_cliente'></span>
                                    </div>
                                        <input type='text' class='form-control' aria-describedby='modi_id_cliente' id='modi_codigo_corto' placeholder='Codigo corto'>
                                </div>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <label for='modi_nombre_cliente' class='col-sm-4 col-form-label'>Nombre</label>
                            <div class='col-sm-8'>
                                <input type='text' class='form-control' id='modi_nombre_cliente' placeholder='Nombre' required>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <label for='modi_rfc_cliente' class='col-sm-4 col-form-label'>RFC</label>
                            <div class='col-sm-8'>
                                <input type='text' class='form-control' id='modi_rfc_cliente' placeholder='RFC' required>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <label for='modi_vencimiento_cliente' class='col-sm-4 col-form-label'>Dias</label>
                            <div class='col-sm-8'>
                                <input type='text' class='form-control' id='modi_vencimiento_cliente' placeholder='Dias' required>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <label for='modi_agrupacion_cliente' class='col-sm-4 col-form-label'>Agrupación</label>
                            <div class='col-sm-8' >
                                <select class="form-control" id="modi_agrupacion_cliente" required>

                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>
                    <button type='submit' class='btn btn-primary'>Guardar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- MODAL    -->


<script>
    $(document).ready(function() {
        Traerclientescartera();
        TraerClasificaciones("","#modi_agrupacion_cliente");
    });

    function Traerclientescartera() {
        // var id = parseInt($('#modal_id_historia').val());

        // destruir_tablaCartasMasivas();

        $('#tabla_catalago_cartera').DataTable({
            destroy: true,
            ajax: {
                "url": "funciones/Traerclientescartera_catalogo.php",
                "dataSrc": ""
            },
            columns: [{
                    "data": "id"
                },
                {
                    "data": "codigo"
                },
                {
                    "data": "codigo_corto"
                },
                {
                    "data": "cliente"
                },
                {
                    "data": "rfc"
                },
                {
                    "data": "vencimiento"
                },
                {
                    "data": "agrupacion"
                },
                {
                    render: function(data, type, row) {
                        return "<div class='btn-group' role='group'><button type='button' class='btn btn-warning btn_small modificar'><i class='fas fa-pencil-alt'></i></button></div>";
                    }
                }
            ],
            autoWidth: true,
            "searching": true,
            "paging": true,
            "info": true,
            language: idioma_espanol,
            order: [
                [0, "asc"]
            ],
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });
    }

    // $("#new_vendedores").on("click", function() {

    //     $('.modal-header').html("<h5 class='modal-title'>Agregar un Vendedor</h5>\
    //                 <button type='button' class='close' data-dismiss='modal' aria-label='Close'>\
    //                     <span aria-hidden='true'>&times;</span>\
    //                 </button>");

    //     $('.modal-body').html("<form class='form-horizontal'>\
    //                     <div class='form-group row'>\
    //                         <label for='new_nombre_vendedor' class='col-sm-4 col-form-label'>Nombre</label>\
    //                         <div class='col-sm-8'>\
    //                             <input type='text' class='form-control' id='new_nombre_vendedor' placeholder='Nombre'>\
    //                         </div>\
    //                     </div>\
    //                     <div class='form-group row'>\
    //                         <label for='new_apellidop_vendedor' class='col-sm-4 col-form-label'>Apellido Paterno</label>\
    //                         <div class='col-sm-8'>\
    //                             <input type='text' class='form-control' id='new_apellidop_vendedor' placeholder='Apellido Paterno'>\
    //                         </div>\
    //                     </div>\
    //                     <div class='form-group row'>\
    //                         <label for='new_apellidom_vendedor' class='col-sm-4 col-form-label'>Apellido Materno</label>\
    //                         <div class='col-sm-8'>\
    //                             <input type='text' class='form-control' id='new_apellidom_vendedor' placeholder='Apellido Materno'>\
    //                         </div>\
    //                     </div>\
    //                 </form>");
    //     $('.modal-footer').html("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Cerrar</button>\
    //                 <button id='Agregar_vendedor' onclick='Agregar_vendedor();' type='button' class='btn btn-primary'>Agregar</button>");

    //     $('#modal_vendedores').modal('toggle');
    // });



    // $(document).on('click', '.eliminar', function() {
    //     var id_cliente = $(this).parents("tr").find("td:eq(0)").html();

    //     Swal.fire({
    //         title: 'Inhabilitar',
    //         text: "Estas seguro que deseas inhabilitar este cliente?",
    //         icon: 'warning',
    //         position: 'top',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Si, inhabilitar'
    //     }).then((result) => {
    //         if (result.value) {

    //             $.ajax({
    //                 url: "funciones/CRUD_clientes_cartera.php",
    //                 type: "POST",
    //                 data: {
    //                     "id_cliente": id_cliente,
    //                     "modo": "Baja"
    //                 },
    //                 success: function(datas) {
    //                     $('#modal_catalogo_cartera').modal('hide');
    //                     Traerclientescartera();
    //                 }
    //             });


    //             Swal.fire({
    //                 position: 'top',
    //                 icon: 'success',
    //                 title: 'Inhabilitado',
    //                 text: 'Este cliente ha sido inhabilitado !',
    //                 showConfirmButton: false,
    //                 timer: 1500
    //             })
    //         }
    //     })


    // });


    $(document).on('click', '.modificar', function() {
        var id = $(this).parents("tr").find("td:eq(0)").html();

        // agrupacion.replace(/[^a-zA-Z\s]/gi, '');
        $.ajax({
            url: "funciones/TraerInfoClienteCartera.php",
            type: "POST",
            data: {
                "id": id
            },
            success: function(datas) {
                
                $("#modi_id_cliente").text(datas.id);
                $("#modi_codigo_corto").val(datas.codigo_corto);
                $("#modi_nombre_cliente").val(datas.cliente);
                $("#modi_rfc_cliente").val(datas.rfc);
                $("#modi_vencimiento_cliente").val(datas.vencimiento);
                $("#modi_agrupacion_cliente").val(datas.id_clasificacion);

            }
        });

        $('#modal_catalogo_cartera').modal('toggle');

    });

    // function traeragrupacion(valor){

    //     var retornar = "";

    //     $.ajax({
    //         url: "funciones/Agrupaciones_clientes_cartera.php",
    //         type: "POST",
    //         data: {
    //             "valor": valor
    //         },
    //         success: function(datas) {
    //             retornar = datas;
    //         }
    //     });


    //     return retornar;

    // }

    // $(document).on('click', '#Agregar_vendedor', function() {
    //     function Agregar_vendedor(){
    //     var nombre = $("#new_nombre_vendedor").val();
    //     var apellidop = $("#new_apellidop_vendedor").val();
    //     var apellidom = $("#new_apellidom_vendedor").val();

    //     $.ajax({
    //         url: "funciones/CRUD_vendedores.php",
    //         type: "POST",
    //         data: {
    //             "nombre": nombre,
    //             "apellidop": apellidop,
    //             "apellidom": apellidom,
    //             "modo": "Alta"
    //         },
    //         success: function(datas) {
    //             $('#modal_vendedores').modal('hide');
    //             Traerclientescartera();
    //         }
    //     });

    // }
    // });

    function TraerClasificaciones(valor,select){

        $(select).empty(); 

        $.ajax({
        url: "funciones/TraerClasificacionesClienteCartera.php",
        success: function(datas) {
            $.each(datas,function(key, registro) {
                $(select).append('<option value='+registro.id+'>'+registro.descripcion+'</option>'); 
            });       
            $(select).val(valor);
        }
        });
    }

    $(document).on('submit', '#Form_catalago_cartera', function() {
        var id_cliente = $("#modi_id_cliente").text();
        var codigo_corto = $("#modi_codigo_corto").val();
        var nombre = $("#modi_nombre_cliente").val();
        var rfc = $("#modi_rfc_cliente").val();
        var vencimiento = $("#modi_vencimiento_cliente").val();
        var clasificacion = $("#modi_agrupacion_cliente ").val();

        $.ajax({
            url: "funciones/CRUD_clientes_cartera.php",
            type: "POST",
            data: {
                "id_cliente": id_cliente,
                "codigo_corto": codigo_corto,
                "nombre": nombre,
                "rfc": rfc,
                "vencimiento": vencimiento,
                "clasificacion": clasificacion,
                "modo": "Modificar"
            },
            success: function(datas) {
                $('#modal_catalogo_cartera').modal('hide');
                Traerclientescartera();
            }
        });


    });

    var idioma_espanol = {
        "decimal": "",
        "emptyTable": "No hay datos disponibles",
        "info": "Viendo _START_ a _END_ de _TOTAL_ registros",
        "infoEmpty": "Viendo 0 a 0 de 0 registros",
        "infoFiltered": "(Filtrado de _MAX_ registros totales)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Ver _MENU_ registros",
        "loadingRecords": "Cargando...",
        "processing": "Procensando...",
        "search": "Buscar:",
        "zeroRecords": "No se encontraron registros",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        },
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        }
    }
</script>
