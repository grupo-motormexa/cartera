# Cartera

![logo](/img/grupo_big.png)

## Add

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```bash
cd existing_repo
git remote add origin https://gitlab.com/grupo-motormexa/cartera.git
git branch -M main
git push -uf origin main
```

## Name

Repositorio del proyecto cartera web  grupo motormexa

## Visuals

Diseño DB:

![Esquema DB](/img/db.PNG)

## Authors and acknowledgment

## Project status
