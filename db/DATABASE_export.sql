-- phpMyAdmin SQL Dump
-- version 4.0.10deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-03-2024 a las 17:55:24
-- Versión del servidor: 5.5.62-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cartera_2024`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`soporte`@`%` PROCEDURE `Crear_corte_table`(IN `empresa_val` VARCHAR(2), IN `fecha_val` DATE)
    NO SQL
BEGIN
	DROP TEMPORARY TABLE IF EXISTS TEMP_Cortes_mes;
	IF empresa_val = '00' THEN
		CREATE TEMPORARY TABLE TEMP_Cortes_mes (SELECT f.id, 
        CASE RIGHT(f.serie,1) 
        WHEN 'L' THEN 'LAGOS'
        WHEN 'C' THEN 'VALLARTA'
 		WHEN 'A' THEN 'ACUEDUCTO'
        WHEN 'V' THEN 'VALLARTA'
		ELSE 'SIN SUCURSAL'
		END
        AS 'Sucursal', v.nombre_corto as vendedor, ul.comentario AS motivo,
        f.serie, f.folio, c.codigo_corto, c.cliente, f.id_empresa, f.fecha_pago, f.estatus, f.fecha_fiscal, DATE_ADD(f.fecha_fiscal, INTERVAL c.vencimiento DAY) AS fecha_vencimiento, ag.nombre AS clasificacion, f.importe_total, f.abonos AS abonos_factura, (f.importe_total- f.abonos) AS saldo, IFNULL(ab.abonos,0) AS abonos, IFNULL(nc.notas,0) AS notas
		FROM `base_reporte_general` br
		LEFT JOIN factura f ON f.codigo_corto = br.codigo_corto AND f.serie = br.serie AND f.id_empresa = br.id_empresa
		LEFT JOIN (SELECT codigo_corto, cliente, id_clasificacion, vencimiento FROM clientes GROUP BY codigo_corto) c ON f.codigo_corto = c.codigo_corto
		LEFT JOIN agrupacion ag ON ag.id = c.id_clasificacion
        LEFT JOIN vendedores v ON v.clave = f.vendedor
        LEFT JOIN (SELECT MAX(id), id_factura, comentario FROM ultimo_seguimiento GROUP BY id_factura) ul ON ul.id_factura = f.id                                        
		LEFT JOIN (SELECT serie, folio, id_empresa, SUM(importe) AS abonos FROM abonos_enterprise_separados WHERE fecha >= fecha_val AND tipo = 'ABONO' GROUP BY serie, folio, id_empresa) ab ON ab.serie = f.serie AND ab.folio = f.folio AND ab.id_empresa = f.id_empresa
		LEFT JOIN (SELECT id_factura, SUM(importe) AS notas FROM nota_credito WHERE fecha < fecha_val AND estatus = 1 GROUP BY id_factura) nc ON nc.id_factura = f.id
		WHERE f.fecha_fiscal < fecha_val);
	ELSE
		CREATE TEMPORARY TABLE TEMP_Cortes_mes (SELECT f.id, 
        CASE RIGHT(f.serie,1) 
        WHEN 'L' THEN 'LAGOS'
        WHEN 'C' THEN 'VALLARTA'
 		WHEN 'A' THEN 'ACUEDUCTO'
        WHEN 'V' THEN 'VALLARTA'
        ELSE 'SIN SUCURSAL'                                        
		END
        AS 'Sucursal', v.nombre_corto as vendedor, ul.comentario AS motivo,
        f.serie, f.folio, c.codigo_corto, c.cliente, f.id_empresa, f.fecha_pago, f.estatus, f.fecha_fiscal, DATE_ADD(f.fecha_fiscal, INTERVAL c.vencimiento DAY) AS fecha_vencimiento, ag.nombre AS clasificacion, f.importe_total, f.abonos AS abonos_factura, (f.importe_total- f.abonos) AS saldo, IFNULL(ab.abonos,0) AS abonos, IFNULL(nc.notas,0) AS notas
		FROM `base_reporte_general` br
		LEFT JOIN factura f ON f.codigo_corto = br.codigo_corto AND f.serie = br.serie AND f.id_empresa = br.id_empresa
		LEFT JOIN (SELECT codigo_corto, cliente, id_clasificacion, vencimiento FROM clientes GROUP BY codigo_corto) c ON f.codigo_corto = c.codigo_corto
		LEFT JOIN agrupacion ag ON ag.id = c.id_clasificacion
        LEFT JOIN vendedores v ON v.clave = f.vendedor
        LEFT JOIN (SELECT MAX(id), id_factura, comentario FROM ultimo_seguimiento GROUP BY id_factura) ul ON ul.id_factura = f.id 
		LEFT JOIN (SELECT serie, folio, id_empresa, SUM(importe) AS abonos FROM abonos_enterprise_separados WHERE fecha >= fecha_val AND tipo = 'ABONO' GROUP BY serie, folio, id_empresa) ab ON ab.serie = f.serie AND ab.folio = f.folio AND ab.id_empresa = f.id_empresa
		LEFT JOIN (SELECT id_factura, SUM(importe) AS notas FROM nota_credito WHERE fecha < fecha_val AND estatus = 1 GROUP BY id_factura) nc ON nc.id_factura = f.id
		WHERE f.id_empresa = empresa_val AND f.fecha_fiscal < fecha_val);	
	END IF;
END$$

CREATE DEFINER=`soporte`@`%` PROCEDURE `ObtenerAbonos`(IN `inicio` VARCHAR(12) CHARSET utf8, IN `fin` VARCHAR(12) CHARSET utf8, IN `serie` VARCHAR(2) CHARSET utf8, IN `empresa` VARCHAR(2) CHARSET utf8, IN `codigo` VARCHAR(5) CHARSET utf8, OUT `abonos` DECIMAL(10,2))
    NO SQL
BEGIN
SELECT SUM(a.importe) INTO abonos FROM `abonos_enterprise_separados` a
LEFT JOIN factura b ON b.id_empresa = a.id_empresa AND b.serie = a.serie AND a.folio = b.folio
WHERE (a.fecha >= inicio AND a.fecha < fin) AND a.id_empresa = empresa AND b.codigo_corto = codigo AND a.serie = serie AND a.tipo = 'ABONO'
GROUP BY b.codigo_corto, b.id_empresa, b.serie;
END$$

CREATE DEFINER=`soporte`@`%` PROCEDURE `ObtenerAbonos_Separados`(IN `inicio` VARCHAR(12) CHARSET utf8, IN `fin` VARCHAR(12) CHARSET utf8, IN `serie` VARCHAR(2) CHARSET utf8, IN `empresa` VARCHAR(2) CHARSET utf8, IN `codigo` VARCHAR(5) CHARSET utf8)
    NO SQL
BEGIN
DROP TEMPORARY TABLE IF EXISTS TEMP_Abonos_Separados;
IF serie = 'SV' THEN
    CREATE TEMPORARY TABLE TEMP_Abonos_Separados (SELECT a.* FROM `abonos_enterprise_separados` a
LEFT JOIN factura b ON b.id_empresa = a.id_empresa AND b.serie = a.serie AND a.folio = b.folio
WHERE (a.fecha >= inicio AND a.fecha < fin) AND a.id_empresa = empresa AND b.codigo_corto = codigo AND a.serie IN ('SV','BV','GV') AND a.tipo = 'ABONO');
    ELSE
    CREATE TEMPORARY TABLE TEMP_Abonos_Separados (SELECT a.* FROM `abonos_enterprise_separados` a
LEFT JOIN factura b ON b.id_empresa = a.id_empresa AND b.serie = a.serie AND a.folio = b.folio
WHERE (a.fecha >= inicio AND a.fecha < fin) AND a.id_empresa = empresa AND b.codigo_corto = codigo AND a.serie = serie AND a.tipo = 'ABONO');
END IF;
END$$

CREATE DEFINER=`soporte`@`%` PROCEDURE `ObtenerContabilidad`(IN `mes` INT, IN `anio` INT)
BEGIN
	SELECT a.codigo_corto, a.serie, b.total_nota FROM factura a
	LEFT JOIN (SELECT id_factura, SUM(importe) AS total_nota FROM nota_credito WHERE MONTH(fecha) = mes AND YEAR(fecha) = anio GROUP BY id_factura) b ON b.id_factura = a.id
	WHERE MONTH(factura.fecha_fiscal) = mes 
	AND YEAR(factura.fecha_fiscal) = anio;
END$$

CREATE DEFINER=`soporte`@`%` PROCEDURE `ObtenerNotasCredito`(IN `inicio` VARCHAR(12) CHARSET utf8, IN `fin` VARCHAR(12) CHARSET utf8, IN `serie` VARCHAR(2) CHARSET utf8, IN `empresa` VARCHAR(2) CHARSET utf8, IN `codigo` VARCHAR(5) CHARSET utf8, OUT `notas` DECIMAL(10,2))
BEGIN
SELECT SUM(a.importe) INTO notas FROM `nota_credito` a
LEFT JOIN factura b ON b.id = a.id_factura
WHERE (a.fecha >= inicio AND a.fecha < fin) AND b.id_empresa = empresa AND b.serie = serie AND b.codigo_corto = codigo
GROUP BY b.codigo_corto, b.id_empresa, b.serie;
END$$

CREATE DEFINER=`soporte`@`%` PROCEDURE `ObtenerNotasCredito_Separados`(IN `inicio` VARCHAR(12) CHARSET utf8, IN `fin` VARCHAR(12) CHARSET utf8, IN `serie` VARCHAR(2) CHARSET utf8, IN `empresa` VARCHAR(2) CHARSET utf8, IN `codigo` VARCHAR(5) CHARSET utf8)
    NO SQL
BEGIN
DROP TEMPORARY TABLE IF EXISTS TEMP_NotasCredito_Separados;
IF serie = 'SV' THEN
    CREATE TEMPORARY TABLE TEMP_NotasCredito_Separados (SELECT a.* FROM `nota_credito` a
LEFT JOIN factura b ON b.id = a.id_factura
WHERE (a.fecha >= inicio AND a.fecha < fin) AND b.id_empresa = empresa AND b.serie IN ('SV','BV','GV') AND b.codigo_corto = codigo);
    ELSE
    CREATE TEMPORARY TABLE TEMP_NotasCredito_Separados (SELECT a.* FROM `nota_credito` a
LEFT JOIN factura b ON b.id = a.id_factura
WHERE (a.fecha >= inicio AND a.fecha < fin) AND b.id_empresa = empresa AND b.serie = serie AND b.codigo_corto = codigo);
END IF;
END$$

CREATE DEFINER=`soporte`@`%` PROCEDURE `ObtenerTotalFacturas`(IN `inicio` VARCHAR(12) CHARSET utf8, IN `fin` VARCHAR(12) CHARSET utf8, IN `serie` VARCHAR(2) CHARSET utf8, IN `empresa` VARCHAR(2) CHARSET utf8, IN `codigo` VARCHAR(5) CHARSET utf8, OUT `facturas` DECIMAL(10,2))
    NO SQL
BEGIN
SELECT SUM(a.importe_total) INTO facturas FROM `factura` a
WHERE (a.fecha_fiscal >= inicio AND a.fecha_fiscal < fin) AND a.id_empresa = empresa AND a.serie = serie AND a.codigo_corto = codigo
GROUP BY a.codigo_corto, a.id_empresa, a.serie;
END$$

CREATE DEFINER=`soporte`@`%` PROCEDURE `ObtenerTotalFacturas_Separados`(IN `inicio` VARCHAR(12) CHARSET utf8, IN `fin` VARCHAR(12) CHARSET utf8, IN `serie` VARCHAR(2) CHARSET utf8, IN `empresa` VARCHAR(2) CHARSET utf8, IN `codigo` VARCHAR(5) CHARSET utf8)
    NO SQL
BEGIN
DROP TEMPORARY TABLE IF EXISTS TEMP_TotalFacturas_Separados;
IF serie = 'SV' THEN
    CREATE TEMPORARY TABLE TEMP_TotalFacturas_Separados (SELECT * FROM `factura` a
	WHERE (a.fecha_fiscal >= inicio AND a.fecha_fiscal < fin) AND a.id_empresa = empresa AND a.serie IN ('SV','BV','GV') AND a.codigo_corto = codigo);
    ELSE
    CREATE TEMPORARY TABLE TEMP_TotalFacturas_Separados (SELECT * FROM `factura` a
	WHERE (a.fecha_fiscal >= inicio AND a.fecha_fiscal < fin) AND a.id_empresa = empresa AND a.serie = serie AND a.codigo_corto = codigo);
END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abonos_notas`
--

CREATE TABLE IF NOT EXISTS `abonos_notas` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `id_factura` int(11) NOT NULL COMMENT 'Id relacion a la factura principal',
  `serie` varchar(4) DEFAULT NULL COMMENT 'Serie del complemento de pago o nota de credito',
  `folio` varchar(8) DEFAULT NULL COMMENT 'Folio del complemento de pago o nota de credito',
  `fecha` date NOT NULL COMMENT 'Fecha del movimiento (fiscal o no fiscal)',
  `importe` decimal(10,2) NOT NULL COMMENT 'Importe del movimiento, nota de credito, complemento, etc',
  `folio_bbj` varchar(7) DEFAULT NULL COMMENT 'Folio interno del BBJ, puede o no puede aplicar',
  `estatus` int(11) NOT NULL DEFAULT '1' COMMENT 'Estatus 1 Activa, 0 Inactiva',
  `fecha_actual` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha que se inserta en la tabla (Se genera automaticamente)',
  `tipo` int(11) NOT NULL COMMENT 'Se refiere si es Nota, Complemente, Cancelada, etc, aún no tengo numeros',
  PRIMARY KEY (`id`),
  KEY `id_factura3` (`id_factura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actualizaciones`
--

CREATE TABLE IF NOT EXISTS `actualizaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `descripcion` varchar(50) NOT NULL COMMENT 'Nombre, descripción del movimiento o actualización',
  `estatus` enum('Activo','Inactivo') NOT NULL COMMENT 'Estatus ''Activo'' ''Inactivo''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adicionales_factura`
--

CREATE TABLE IF NOT EXISTS `adicionales_factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `id_factura` int(11) NOT NULL COMMENT 'Id factura relacion principal',
  `id_actualizacion` int(11) NOT NULL COMMENT 'Id relacion a la tabla movimientos para saber que actualizo',
  `valor` varchar(40) DEFAULT NULL COMMENT 'Campo libre para colocar por ejemplo zona u otros campos adicionales',
  `fecha` date NOT NULL COMMENT 'Fecha del movimiento u actualizacion',
  PRIMARY KEY (`id`),
  KEY `id_factura` (`id_factura`),
  KEY `indice_actualizacion` (`id_actualizacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clasificacion`
--

CREATE TABLE IF NOT EXISTS `clasificacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `nombre` varchar(40) NOT NULL COMMENT 'Nombre, descripción del tipo de clasificación',
  `estatus` enum('Activo','Inactivo') NOT NULL COMMENT 'Estatus ''Activo'', ''Inactivo''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `clasificacion`
--

INSERT INTO `clasificacion` (`id`, `nombre`, `estatus`) VALUES
(1, 'CASA CUERVO', 'Activo'),
(2, 'ASEGURADORAS', 'Activo'),
(3, 'DEPENDENCIAS DE GOBIERNO', 'Activo'),
(4, 'OTROS', 'Activo'),
(5, 'EMPLEADOS', 'Activo'),
(6, 'CLIENTES', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `num_cliente` varchar(11) NOT NULL COMMENT 'Se refiere al codigo de Visual, es un código interno',
  `codigo_corto` varchar(6) NOT NULL COMMENT 'Codigo de 6 digitos para agrupar a los clientes, el mismo codigo de visual pero sin las R, las S etc',
  `cliente` varchar(200) NOT NULL COMMENT 'Nombre del cliente',
  `rfc` varchar(13) DEFAULT NULL COMMENT 'RFC del cliente',
  `vencimiento` int(11) DEFAULT NULL COMMENT 'Dias que tiene de credito, 30, 60, 90 etc',
  `id_clasificacion` int(11) DEFAULT '6' COMMENT 'Id relacion a clasificaciones',
  PRIMARY KEY (`id`),
  KEY `num_cliente` (`num_cliente`),
  KEY `id` (`id`),
  KEY `codigo_corto` (`codigo_corto`),
  KEY `fk_id_clasificacion` (`id_clasificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `id_factura` int(11) NOT NULL COMMENT 'Id relacion a la factura principal',
  `usuario` varchar(20) NOT NULL COMMENT 'Usuario que agrego el comentario para tener registro',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha que se inserta en la tabla (Se genera automaticamente)',
  `comentario` varchar(500) DEFAULT NULL COMMENT 'Comentario del seguimiento que se esta dando a la factura',
  PRIMARY KEY (`id`),
  KEY `id_factura` (`id_factura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE IF NOT EXISTS `estatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `nombre` varchar(100) NOT NULL COMMENT 'Nombre, descripción del tipo de estatus, 1 emitida, 2 recibida en cxc, 3 en portal, 4 pagada, 5 nota de credito, 6 cancelada, 7 abonada, 8 con saldo a favor ',
  `estatus` enum('Activo','Inactivo') NOT NULL COMMENT 'Estatus ''Activo'', ''Inactivo''',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE IF NOT EXISTS `factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `id_empresa` varchar(2) CHARACTER SET utf8 NOT NULL COMMENT 'Id empresa, para saber si la factura es de grupo 01 o de automotriz 02',
  `serie` varchar(4) CHARACTER SET utf8 NOT NULL COMMENT 'Serie fiscal de la factura SV, RV, SA, etc',
  `folio` varchar(8) CHARACTER SET utf8 NOT NULL COMMENT 'Folio fiscal de la factura con mascara de 8 digitos ejemplo ''00000034''',
  `vendedor` varchar(6) CHARACTER SET latin1 NOT NULL COMMENT 'Id vendedor que hace relación a la tabla de vendedores, a 6 digitos',
  `importe_total` decimal(10,2) NOT NULL COMMENT 'Importe total de la factura fiscal',
  `factura_bbj` varchar(7) CHARACTER SET latin1 NOT NULL COMMENT 'Folio interno de BBJ a 7 digitos',
  `fecha_fiscal` date NOT NULL COMMENT 'Fecha fiscal de la factura ''YYYY-MM-DD''',
  `codigo_corto` varchar(6) CHARACTER SET latin1 DEFAULT NULL COMMENT 'Codigo de 6 digitos para relacionarlo con el cliente',
  `estatus` int(11) NOT NULL DEFAULT '1' COMMENT 'Estatus de la factura, 1 emitida, 2 recibida, 3 en portal, 4 pagada, 5 nota credito, 6 cancelada, 7 abonada, 8 saldo a favor, deberia cambiar automaticamente con un trigger',
  `fecha_carga` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha que se inserta en la tabla (Se genera automaticamente)',
  PRIMARY KEY (`id`),
  KEY `id_estatus` (`estatus`),
  KEY `vendedores` (`vendedor`),
  KEY `codigo_corto` (`codigo_corto`)
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE IF NOT EXISTS `pedidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `id_factura` int(11) NOT NULL COMMENT 'Id relacion a la factura principal',
  `num_pedido` int(11) NOT NULL COMMENT 'Numero de pedido interno de BBJ',
  `fecha_pedido` date NOT NULL COMMENT 'Fecha del pedido',
  `siniestro` varchar(50) NOT NULL COMMENT 'Numero de siniesto interno de BBJ',
  `importe` decimal(10,2) NOT NULL COMMENT 'Importe del siniestro o pedido',
  PRIMARY KEY (`id`),
  KEY `id_factura_d` (`id_factura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedores`
--

CREATE TABLE IF NOT EXISTS `vendedores` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Indice autoincremental',
  `clave` varchar(6) NOT NULL COMMENT 'Clave del vendedor a 6 digitos, sirve para hacer la unión con la factura',
  `nombre` varchar(50) NOT NULL COMMENT 'Nombre Completo del vendedor',
  `nombre_corto` varchar(100) DEFAULT NULL COMMENT 'Nombre corto del vendedor para temas de reportes',
  PRIMARY KEY (`id`),
  KEY `clave` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `adicionales_factura`
--
ALTER TABLE `adicionales_factura`
  ADD CONSTRAINT `fk_actualizaciones` FOREIGN KEY (`id_actualizacion`) REFERENCES `actualizaciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_factura` FOREIGN KEY (`id_factura`) REFERENCES `factura` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `fk_clasificacion` FOREIGN KEY (`id_clasificacion`) REFERENCES `clasificacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `fk_id_fac_comentario` FOREIGN KEY (`id_factura`) REFERENCES `factura` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`id`) REFERENCES `abonos_notas` (`id_factura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_codigo_corto` FOREIGN KEY (`codigo_corto`) REFERENCES `clientes` (`codigo_corto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_estatus` FOREIGN KEY (`estatus`) REFERENCES `estatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vendedor` FOREIGN KEY (`vendedor`) REFERENCES `vendedores` (`clave`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `fk_pedidos` FOREIGN KEY (`id_factura`) REFERENCES `factura` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
